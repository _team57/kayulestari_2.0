<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
	function generate_pdf($html, $filename='', $paper, $orientation, $stream=TRUE)
	{
		require_once APPPATH."helpers/dompdf/dompdf_config.inc.php";
		//
		$dompdf = new DOMPDF();
		$dompdf->set_paper($paper,$orientation);
		$dompdf->load_html($html);
		$dompdf->render();
		//
		if ($stream == TRUE)
			$dompdf->stream($filename);
		else
			return $dompdf->output();
	}
?>