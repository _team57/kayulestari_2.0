<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();

	/**
	* @dzafiars
	*/
class Anggota extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('AnggotaModel', '', TRUE);
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');

        if ($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
        }
    }

    public function index(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('js-include/anggotaList-js', $data, true);

                $data['useDatatable'] = true;

                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('anggotaList', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('home', 'refresh');
            }    		
        } else {
            redirect('auth', 'refresh');
        }
    }
 
    public function ajaxAnggotaDataTable(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];

                $order = 'no_anggota';
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'no_anggota';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'koperasi';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'nama';
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = 'tanggal_masuk';
                } 

                $data = $this->AnggotaModel->findAllWithPaging($offset, $limit, $sVal, $order, $dir);
                $totalResult = $this->AnggotaModel->getTotalResult($offset, $limit, $sVal, $order, $dir);

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
   
    public function ajaxCreateAnggota(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $anggota = new AnggotaModel();
                $anggota->username = $this->input->post('username');
                $anggota->name = $this->input->post('name');
                $password = "TOKEN:".$anggota->username.$this->input->post('password');
                $hashPassword = sha1($password);
                $anggota->password = $hashPassword;
                $anggota->role_id = $this->input->post('role_id');
                $anggota->spv_id = $this->input->post('spv_id');

                $result = $anggota->save();
                
                if($result){
                    $data['msg'] = 'Anggota added';
                    $data['status'] = 'SUCCESS';
                }else{
                    $data['msg'] = 'Failed to save inventory';
                    $data['status'] = 'ERROR';
                }
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    function ajaxGetAnggota(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($this->input->post('no_anggota')) {
                if ($session['role_id']  > 0) {
                    header("Content-Type: application/json");
                    $anggota = $this->AnggotaModel->findByNoAnggota($this->input->post('no_anggota'));

                    if ($anggota) {
                        $data['msg'] = 'Anggota telah didapat.';
                        $data['status'] = 'SUCCESS';
                        $data['data'] = $anggota;
                    } else {
                        $data['msg'] = 'Anggota tidak ditemukan di database.';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['status'] = 'ERROR';
                    $data['msg'] = 'Unauthorized';

                    header('HTTP/1.1 401 Unauthorized', true, 401);
                }
            } else {
                $data['msg'] = 'Parameter Kurang';
                $data['status'] = 'ERROR';
                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    public function ajaxUpdateAnggota(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $anggota = $this->AnggotaModel->findById($this->input->post('id'));
                if ($anggota) {
                    $anggota = new AnggotaModel();
                    $anggota->username = $this->input->post('username');
                    $password = "TOKEN:".$anggota->username.$this->input->post('password');
                    $hashPassword = sha1($password);
                    $anggota->password = $hashPassword;
                    $anggota->name = $this->input->post('name');
                    $anggota->role_id = $this->input->post('role_id');
                    $anggota->spv_id = $this->input->post('spv_id');
                    $anggota->id = $this->input->post('id');
                    
                    $result = $anggota->update();
                    
                    if ($result) {
                        $data['msg'] = 'Anggota telah diupdate';
                        $data['status'] = 'SUCCESS';
                    } else {
                        $data['msg'] = 'Anggota gagal diupdate';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['msg'] = 'Anggota tidak ditemukan di database.';
                    $data['status'] = 'ERROR';
                }	            
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    public function ajaxDeleteAnggota(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $id = $this->input->post('id');
                $deleteStatus = $this->AnggotaModel->deleteById($id);

                if ($deleteStatus) {
                    $data['msg'] = 'Anggota telah dihapus.';
                    $data['status'] = 'SUCCESS';
                } else {
                    $data['msg'] = 'Anggota gagal dihapus';
                    $data['status'] = 'ERROR';
                }
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }

}
?>