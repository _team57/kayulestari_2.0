<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_start();

/**
 * 
 */
class Auth extends CI_Controller {

    function __construct() {
        parent::__construct();
        
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');
        $this->load->model('UserModel', '', TRUE);
    }

    public function index() {
        redirect('auth/login', 'refresh');
    }
    
    public function login() {
        if ($this->session->userdata('logged_in')) {
            redirect('dashboard', 'refresh');
        } else {
            $this->load->view('login');
        }
    }

    public function postLogin() {
        $redirectURL = 'dashboard';
        if ($this->input->post('redirectURL') != "") {
            $redirectURL = urldecode($this->input->post('redirectURL'));
        }
        $this->load->library('form_validation');

        if ($this->session->userdata('logged_in')) {
            redirect($redirectURL, 'refresh');
        } else {
            $username = $this->input->post('username');
            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_validation');
            if ($this->form_validation->run() == FALSE) {

                //redirect('auth/login', 'refresh');
                $data['title'] = "Sign In";
                $data['redirectURL'] = $redirectURL;
                $this->load->view('login', $data);
            } else {
                $session = $this->session->userdata('logged_in');
                if ($this->session->userdata('admin_logged_in')) {
                    $sessionAdmin = $this->session->userdata('admin_logged_in');
                    $userId = $sessionAdmin['userId'];
                    $this->session->unset_userdata('admin_logged_in');
                    session_destroy();
                }
                redirect($redirectURL, 'refresh');
            }
        }
        // $pass = $this->input->post('password');
        // $pass1 = hash('sha1',$this->input->post('password'));
        // $this->load->view('form-login', array('pass' => $pass, 'pass1' => $pass1));
    }

    public function logout() {
        if ($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
            $user_id = $session['user_id'];

            $this->session->unset_userdata('logged_in');
            session_destroy();

            redirect('auth', 'refresh');
        } else {
            redirect('auth', 'refresh');
        }
    }

    public function changePin() {
        if ($this->session->userdata('logged_in')) {
            header("Content-Type: application/json");
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            // $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_validationWithNoSessionUpdate');
            if ($this->form_validation->run() == FALSE) {
                $data['msg'] = 'Pin salah.';
                $data['status'] = 'ERROR';
            } else {
                $session = $this->session->userdata('logged_in');
                $this->UserModel->setDB("zeepos_client_" . $session['userProfileId']);
                $user = $this->UserModel->findById($this->input->post('id'));
                $user->setDB("zeepos_client_" . $session['userProfileId']);
                if ($user) {
                    $userPassword = "TOKEN:" . $this->input->post('username') . $this->input->post('newPin');
                    $user->password = hash('sha1', $userPassword);

                    if ($user->changePassword($this->input->post('id'))) {
                        $data['msg'] = 'Pin telah diupdate.';
                        $data['status'] = 'SUCCESS';
                    } else {
                        $data['msg'] = 'Pin gagal diupdate';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['msg'] = 'User tidak ditemukan di database.';
                    $data['status'] = 'ERROR';
                }
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';
            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }

    public function changePassword() {
        if ($this->session->userdata('logged_in')) {
            header("Content-Type: application/json");
            $this->load->library('form_validation');

            $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_validationWithNoSessionUpdate');
            if ($this->form_validation->run() == FALSE) {
                $data['msg'] = 'Password salah.';
                $data['status'] = 'ERROR';
            } else {
                $session = $this->session->userdata('logged_in');
                $this->UserModel->setDB("zeepos_client_" . $session['userProfileId']);
                $user = $this->UserModel->findById($this->input->post('id'));
                $user->setDB("zeepos_client_" . $session['userProfileId']);
                if ($user) {
                    $userPassword = "TOKEN:" . $this->input->post('username') . $this->input->post('newPassword');
                    $user->password = hash('sha1', $userPassword);

                    if ($user->changePassword($this->input->post('id'))) {
                        $data['msg'] = 'Password telah diupdate.';
                        $data['status'] = 'SUCCESS';
                    } else {
                        $data['msg'] = 'Password gagal diupdate';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['msg'] = 'User tidak ditemukan di database.';
                    $data['status'] = 'ERROR';
                }
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';
            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }

    public function passwordForgot() {
        $data['useDatatable'] = false;
        $data['extendFooter'] = $this->load->view('js-include/passwordforgot-js', $data, true);

        $this->load->view('include/header', $data);
        // $this->load->view('include/nav');
        $this->load->view('passwordForgot', $data);
        $this->load->view('include/footer', $data);
    }

    public function postPasswordForgot() {
        header("Content-Type: application/json");
        $username = $this->input->post('username');
        if ($username != null && $username != '') {
            $this->ProfileModel->setDB("zeepos_master");
            $profile = $this->ProfileModel->findByEmail($username);

            if ($profile) {
                $this->UserModel->setDB("zeepos_client_" . $profile->id);
                $user = $this->UserModel->findById($profile->owner_id);

                // if ($user) {
                $this->VerificationCodeModel->setDB("zeepos_master");
                $verification = $this->VerificationCodeModel->findByUserId($profile->id);

                $urlReset = site_url("auth/passwordReset/" . $verification->verification_code);

                $mail = new EmailModel();
                // $mail->recipent = 'surat.dev71@gmail.com';
                $mail->recipent = $profile->email;
                $mail->subject = "Reset Password";
                $temp['name'] = $profile->name;
                $temp['urlReset'] = $urlReset;
                // $mail->message = $this->load->view('template/resetPasswordSubscriber', $temp, true);
                $mail->message = $this->load->view('template/resetPasswordSubscriber2', $temp, true);
                $sendMail = $mail->sendmail();
                // $sendMail = true;

                if ($sendMail) {
                    $data['msg'] = 'Petunjuk perubahan password telah dikirim ke Email anda.';
                    $data['status'] = 'SUCCESS';
                } else {
                    $data['msg'] = 'Petunjuk perubahan password gagal dikirim ke Email anda.';
                    $data['status'] = 'ERROR';
                }
                // } else {
                // 	$data['msg'] = 'User tidak ditemukan di database.';
                //              $data['status'] = 'ERROR';
                // }
            } else {
                $data['msg'] = 'User tidak ditemukan di database.';
                $data['status'] = 'ERROR';
            }
        } else {
            $data['msg'] = 'Terjadi kesalahan input.';
            $data['status'] = 'ERROR';
        }
        echo json_encode($data);
    }

    public function passwordReset($verification_code) {
        if (isset($verification_code)) {
            $this->VerificationCodeModel->setDB("zeepos_master");
            $verification = $this->VerificationCodeModel->findByVerificationCode($verification_code);

            if ($verification) {
                $this->ProfileModel->setDB("zeepos_master");
                $profile = $this->ProfileModel->findById($verification->user_id);
                $data['useDatatable'] = false;
                $data['extendFooter'] = $this->load->view('js-include/passwordreset-js', $data, true);

                $this->UserModel->setDB("zeepos_client_" . $profile->id);
                $user = $this->UserModel->findById($profile->owner_id);
                $data['userId'] = $user->id;
                $data['profileId'] = $profile->id;

                $this->load->view('include/header', $data);
                // $this->load->view('include/nav');
                $this->load->view('passwordReset', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('auth/passwordForgot', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }

    public function postPasswordReset() {
        header("Content-Type: application/json");
        $id = $this->input->post('id');
        $profile_id = $this->input->post('profile_id');
        $password = $this->input->post('password');
        if ($id != null && $id != '' && $password != null && $password != '') {
            $this->UserModel->setDB("zeepos_client_" . $profile_id);
            $user = $this->UserModel->findById($id);
            if ($user) {
                $user->setDB("zeepos_client_" . $profile_id);
                $user->password = hash('sha1', "TOKEN:" . $user->username . $password);

                if ($user->changePassword($id)) {
                    $data['msg'] = 'Password telah diubah.';
                    $data['status'] = 'SUCCESS';
                } else {
                    $data['msg'] = 'Password gagal diubah';
                    $data['status'] = 'ERROR';
                }
            } else {
                $data['msg'] = 'User tidak ditemukan di database.';
                $data['status'] = 'ERROR';
            }
        } else {
            $data['msg'] = 'Terjadi kesalahan input.';
            $data['status'] = 'ERROR';
        }
        echo json_encode($data);
    }

    function validation() {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $database = $this->input->post('select_db');
//        echo $database;exit;
        $user = new UserModel();
        $user->setDB($database);
        $user->username = $username;
        $userPassword = "TOKEN:" . $username . $password;
        $user->password = hash('sha1', $userPassword);
        
        $authUser = $user->login();

        if ($authUser) {
            if ($authUser->role_id == 1 || $authUser->role_id == 2 || $authUser->role_id == 3) {
                $sessArray = array(
                    'user_id' => $authUser->id,
                    'username' => $authUser->username,
                    'name' => $authUser->name,
                    'role_id' => $authUser->role_id,
                    'spv_id' => $authUser->spv_id,
                    'select_db' => $database
                );
                $this->session->set_userdata('logged_in', $sessArray);
                return TRUE;
            } else {
                $this->form_validation->set_message('validation', 'You dont have permission to login.');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('validation', 'Invalid username or password.');
            return FALSE;
        }
    }

    function validationWithNoSessionUpdate() {
        if ($this->session->userdata['logged_in']) {
            $session = $this->session->userdata('logged_in');
            $profileId = $session['userProfileId'];
        } else {
            $email = $this->input->post('email');
            $this->ProfileModel->setDB("zeepos_master");
            $profile = $this->ProfileModel->findByEmail($email);
            $profileId = $profile->id;
        }
        $user = new UserModel();
        $user->setDB("zeepos_client_" . $profileId);
        $user->username = $this->input->post('username');
        $user->password = hash('sha1', $this->input->post('password'));
        $authUser = $user->login();

        if (!$authUser) {
            $userPassword = "TOKEN:" . $this->input->post('username') . $this->input->post('password');
            $user->password = hash('sha1', $userPassword);
            $authUser = $user->login();
        }

        if ($authUser) {
            return TRUE;
        } else {
            $this->form_validation->set_message('validation', 'Invalid username or password');
            return FALSE;
        }
    }

    public function ajaxCheckUsername() {
        $response = array(
            'valid' => false,
            'message' => 'Username harus diisi.'
        );

        if (isset($_POST['username'])) {
            $user = $this->UserModel->findByUsername($_POST['username']);
            if ($user) {
                $owner = $this->ProfileModel->findBysubscriberId($user->id);
                if ($owner) {
                    $response = array('valid' => true);
                } else {
                    $response = array('valid' => false, 'message' => 'Anda bukan pemilik atau username yang anda maksud tidak ada.');
                }
            } else {
                $response = array('valid' => false, 'message' => 'Anda bukan pemilik atau username yang anda maksud tidak ada.');
            }
        }
        echo json_encode($response);
    }

    public function ajaxCheckPasswordNotEmpty() {
        $response = array(
            'valid' => false,
            'message' => 'Password harus diisi.'
        );

        if (isset($_POST['password'])) {
            $response = array('valid' => true);
        }
        echo json_encode($response);
    }

    public function ajaxCheckNewPasswordConfirmation($password) {
        $response = array(
            'valid' => false,
            'message' => 'Password konfirmasi harus diisi.'
        );

        if (isset($_POST['password-confirmation'])) {
            $passwordConfirmation = $this->input->post('password-confirmation');
            if ($passwordConfirmation == $password) {
                $response = array('valid' => true);
            } else {
                $response = array(
                    'valid' => false,
                    'message' => 'Password konfirmasi tidak sama dengan password baru.'
                );
            }
        }
        echo json_encode($response);
    }
}

?>