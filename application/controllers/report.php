<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_start();

/**
 * 
 */
class Report extends CI_Controller {

    function __construct() {
        parent::__construct();
        
		$this->load->model('PohonModel', '', TRUE);
		$this->load->model('LahanModel', '', TRUE);
		$this->load->model('UserModel', '', TRUE);

        if ($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
        }
    }

    public function index() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                redirect('report/verifikasiReport', 'refresh');
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
    
    public function verifikasiReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/verifikasi-report-js', $data, true);
                $data['verif'] = $this->UserModel->findAllVerif();
                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d');
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/verifikasiReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxVerifikasiReportDataTable($startDate, $endDate, $type = 0) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                if(stristr($session['select_db'], 'kwlm') === FALSE) {
                    $order = 'u.name';
                } else {
                    $order = 'u.id';
                }
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'name';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'username';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'jumlah_lahan';
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = 'assigned';
                } else if ($reqOrder[0]['column'] == 5) {
                    $order = 'verified_valid';
                } else if ($reqOrder[0]['column'] == 6) {
                    $order = 'verified_invalid';
                } else if ($reqOrder[0]['column'] == 7) {
                    $order = 'unverified';
                }
    
                $data = $this->PohonModel->getVerificationReport($startDate, $endDate, $offset, $limit, $sVal, $order, $dir, $type, 0);
                $totalResult = $this->PohonModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadVerifikasiReport($startDate, $endDate, $type = 0) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');
            
            if(stristr($session['select_db'], 'kwlm') === FALSE) {
                $order = 'u.name';
            } else {
                $order = 'u.id';
            }
            $data = $this->PohonModel->getVerificationReport($startDate, $endDate, 0, 0, '', $order, 'asc', $type);
            $total_pohon = 0;
            $total_verified_valid = 0;
            $total_verified_invalid = 0;
            $total_unverified = 0;
            foreach ($data as $item){
                $total_pohon += $item->assigned;
                $total_verified_valid += $item->verified_valid;
                $total_verified_invalid += $item->verified_invalid;
                $total_unverified += $item->unverified;
            }
            
            if($startDate == '0000-00-00' AND $endDate == '0000-00-00'){
                $range_date = "ALL TIME";
            } else{
                $range_date = date_format(date_create($startDate), "d-m-Y")." - ". date_format(date_create($endDate), "d-m-Y");
            }
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm_v'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) sebelum 2017";
                $file_name = "Laporan Verifikasi KWLM sebelum 2017 ".date("d-m-Y").".xlsx";
            } else if ($session['select_db'] == 'kayulestari_kwlm_prod'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) 2017";
                $file_name = "Laporan Verifikasi KWLM 2017 ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Verifikasi KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Verifikasi");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:H2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:C3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:C4");
            $objPHPExcel->getActiveSheet()->mergeCells("A5:C5");
            $objPHPExcel->getActiveSheet()->mergeCells("A6:C6");
            $objPHPExcel->getActiveSheet()->mergeCells("A7:C7");
            $objPHPExcel->getActiveSheet()->mergeCells("A8:C8");
            $objPHPExcel->getActiveSheet()->mergeCells("D3:H3");
            $objPHPExcel->getActiveSheet()->mergeCells("D4:H4");
            $objPHPExcel->getActiveSheet()->mergeCells("D5:H5");
            $objPHPExcel->getActiveSheet()->mergeCells("D6:H6");
            $objPHPExcel->getActiveSheet()->mergeCells("D7:H7");
            $objPHPExcel->getActiveSheet()->mergeCells("D8:H8");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Range Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D4', $range_date);
            $objPHPExcel->getActiveSheet()->getStyle('D3:D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Jumlah Seluruh Pohon');
            $objPHPExcel->getActiveSheet()->setCellValue('D5', $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Jumlah Verified(Valid)');
            $objPHPExcel->getActiveSheet()->setCellValue('D6', $total_verified_valid);
            $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Jumlah Verified(Invalid)');
            $objPHPExcel->getActiveSheet()->setCellValue('D7', $total_verified_invalid);
            $objPHPExcel->getActiveSheet()->setCellValue('A8', 'Jumlah Unverified');
            $objPHPExcel->getActiveSheet()->setCellValue('D8', $total_unverified);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:H8')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 10;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Username');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jumlah Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Assigned');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Verified(Valid)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Verified(Invalid)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Unverified');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_user);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->username);
                    $numberFormatBefore = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_lahan);
                    $numberFormatStart = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->assigned);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->verified_valid);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->verified_invalid);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->unverified);

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("C".$ii.":".$numberFormatBefore.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($numberFormatStart.$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$lastRow, $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRow, $total_verified_valid);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastRow, $total_verified_invalid);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$lastRow, $total_unverified);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$lastRow.":".'H'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$lastRow.":".'H'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php/transaction");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php/transaction");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
        
    public function lahanReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/lahan-report-js', $data, true);
                $data['pendata'] = $this->UserModel->findAllPendata();
                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d');
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/lahanReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxLahanReportDataTable($pendata) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                $order = 'anggota.nama';
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'no_anggota';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'nama_anggota';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'nama_lahan';
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = 'no_sertifikat';
                } else if ($reqOrder[0]['column'] == 5) {
                    $order = 'jenis_sertifikat';
                } else if ($reqOrder[0]['column'] == 7) {
                    $order = 'luas_lahan';
                } else if ($reqOrder[0]['column'] == 8) {
                    $order = 'user.name';
                }
    
                $data = $this->LahanModel->findLahanForReport($offset, $limit, $sVal, $order, $dir, $pendata);
                $totalResult = $this->LahanModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadLahanReport($pendata) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');
            
            $data = $this->PohonModel->getVerificationReport(0, 0, '', 'anggota.nama', 'asc', $pendata);
            $total_lahan = count($data);
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm_v'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) sebelum 2017";
                $file_name = "Laporan Lahan KWLM sebelum 2017 ".date("d-m-Y").".xlsx";
            } else if ($session['select_db'] == 'kayulestari_kwlm_prod'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) 2017";
                $file_name = "Laporan Lahan KWLM 2017 ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Lahan KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Lahan");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:G2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:B4");
            $objPHPExcel->getActiveSheet()->mergeCells("C3:G3");
            $objPHPExcel->getActiveSheet()->mergeCells("C4:G4");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('C3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->getStyle('C3:C4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Jumlah Seluruh Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue('C4', $total_lahan);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E4')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 6;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'No Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'No Sertifikat');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jenis Sertifikat');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Foto Sertifikat, Sketsa');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Luas Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Pendata');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Kelurahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Kecamatan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Kabupaten');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Provinsi');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Latitude');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Longitude');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Altitude');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_user);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->username);
                    $numberFormatBefore = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_lahan);
                    $numberFormatStart = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->assigned);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->verified);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->unverified);

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("C".$ii.":".$numberFormatBefore.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($numberFormatStart.$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$lastRow, $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRow, $total_verified);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastRow, $total_unverified);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$lastRow.":".'G'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$lastRow.":".'G'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php/transaction");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php/transaction");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
    
    public function fotoLahanReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/foto-lahan-report-js', $data, true);
                
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/fotoLahanReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxFotoLahanReportDataTable() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                
                $order = 'anggota.nama';
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'anggota.no_anggota';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'anggota.nama';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'nama_lahan';
                }
    
                $data = $this->LahanModel->findFotoLahan($offset, $limit, $sVal, $order, $dir);
                $totalResult = $this->LahanModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadFotoLahanReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');
            
            $data = $this->LahanModel->findFotoLahan(0, 0, '', "anggota.nama", "asc");
            $total_foto_sertifikat = 0;
            $total_foto_sketsa = 0;
            $total_data = count($data);
            foreach ($data as $item){
                $total_foto_sertifikat += $item->jumlah_foto_sertifikat;
                $total_foto_sketsa += $item->jumlah_foto_sketsa;
            }
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm_v'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) sebelum 2017";
                $file_name = "Laporan Foto Lahan KWLM sebelum 2017 ".date("d-m-Y").".xlsx";
            } else if ($session['select_db'] == 'kayulestari_kwlm_prod'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) 2017";
                $file_name = "Laporan Foto Lahan KWLM 2017 ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Foto Lahan KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Foto Lahan");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:I2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:B3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:B4");
            $objPHPExcel->getActiveSheet()->mergeCells("A5:B5");
            $objPHPExcel->getActiveSheet()->mergeCells("A6:B6");
            $objPHPExcel->getActiveSheet()->mergeCells("C3:I3");
            $objPHPExcel->getActiveSheet()->mergeCells("C4:I4");
            $objPHPExcel->getActiveSheet()->mergeCells("C5:I5");
            $objPHPExcel->getActiveSheet()->mergeCells("C6:I6");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('C3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->getStyle('C3:C6')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Total Foto Sertifikat');
            $objPHPExcel->getActiveSheet()->setCellValue('C4', $total_foto_sertifikat);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Total Foto Sketsa');
            $objPHPExcel->getActiveSheet()->setCellValue('C5', $total_foto_sketsa);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Total Data');
            $objPHPExcel->getActiveSheet()->setCellValue('C6', $total_data);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I6')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 8;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'No Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Lahan ID');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Foto Sertifikat');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jumlah Foto Sertifikat');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Foto Sketsa');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jumlah Foto Sketsa');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->no_anggota);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->lahan_id);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_lahan);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->foto_sertifikat);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_foto_sertifikat);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->foto_sketsa);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_foto_sketsa);

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("A".$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastRow, $total_foto_sertifikat);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$lastRow, $total_foto_sketsa);
            $objPHPExcel->getActiveSheet()->getStyle('A'.$lastRow.":".'I'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$lastRow.":".'I'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php/transaction");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php/transaction");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
    
    public function pendataanReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/pendataan-report-js', $data, true);
                $data['pendata'] = $this->UserModel->findAllPendata();
                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d');
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/pendataanReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxPendataanReportDataTable($startDate, $endDate, $user_id) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                $order = "DATE_FORMAT(`timestamp`, '%e %M %Y'), pendata";
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = "pendata";
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = "DATE_FORMAT(`timestamp`, '%e %M %Y')";
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = "anggota.no_anggota";
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = "anggota.nama";
                } else if ($reqOrder[0]['column'] == 5) {
                    $order = "lahan.nama_lahan";
                } else if ($reqOrder[0]['column'] == 6) {
                    $order = "awal_pendataan";
                } else if ($reqOrder[0]['column'] == 7) {
                    $order = "akhir_pendataan";
                } else if ($reqOrder[0]['column'] == 8) {
                    $order = "jumlah_pohon";
                }
    
                $data = $this->PohonModel->getPendataanReport($startDate, $endDate, $user_id, $offset, $limit, $sVal, $order, $dir);
                $totalResult = $this->PohonModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadPendataanReport($startDate, $endDate, $user_id) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');

            $data = $this->PohonModel->getPendataanReport($startDate, $endDate, $user_id, 0, 0, '', "DATE_FORMAT(`timestamp`, '%e %M %Y')", 'DESC');
            $total_pohon = 0;
            foreach ($data as $item){
                $total_pohon += $item->jumlah_pohon;
            }
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm_v'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) sebelum 2017";
                $file_name = "Laporan Pendataan KWLM sebelum 2017 ".date("d-m-Y").".xlsx";
            } else if ($session['select_db'] == 'kayulestari_kwlm_prod'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) 2017";
                $file_name = "Laporan Pendataan KWLM 2017 ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Pendataan KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Pendataan");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:I2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:C3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:C4");
            $objPHPExcel->getActiveSheet()->mergeCells("A5:C5");
            $objPHPExcel->getActiveSheet()->mergeCells("D3:I3");
            $objPHPExcel->getActiveSheet()->mergeCells("D4:I4");
            $objPHPExcel->getActiveSheet()->mergeCells("D5:I5");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->getStyle('D3:D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Range Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D4', $startDate." sd ".$endDate);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Jumlah Pohon Terdata');
            $objPHPExcel->getActiveSheet()->setCellValue('D5', $total_pohon);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:E5')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 7;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Pendata');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Tanggal Pendataan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'No Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nma Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Awal Pendataan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Akhir Pendataan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jumlah Pohon');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->pendata);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->tanggal_pendataan);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->no_anggota);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_anggota);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_lahan);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->awal_pendataan);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->akhir_pendataan);
                    $numberFormatBefore = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_pohon);
                    $numberFormatStart = $currentColumnValueIndex;

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("C".$ii.":".$numberFormatBefore.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($numberFormatStart.$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$lastRow, $total_pohon);
            $objPHPExcel->getActiveSheet()->getStyle('I'.$lastRow.":".'I'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('H'.$lastRow.":".'I'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php/transaction");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php/transaction");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================

    public function verifikasiPerLahanReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/verifikasiperlahan-report-js', $data, true);
                $data['verif'] = $this->UserModel->findAllVerif();
                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d');
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/verifikasiPerLahanReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxVerifikasiPerLahanReportDataTable($startDate, $endDate, $type = 0) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                if(stristr($session['select_db'], 'kwlm') === FALSE) {
                    $order = 'u.name';
                } else {
                    $order = 'u.id';
                }
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'pendata';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'no_anggota';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'nama';
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = 'nama_lahan';
                } else if ($reqOrder[0]['column'] == 5) {
                    $order = 'assigned';
                } else if ($reqOrder[0]['column'] == 6) {
                    $order = 'verified_valid';
                } else if ($reqOrder[0]['column'] == 7) {
                    $order = 'verified_invalid';
                } else if ($reqOrder[0]['column'] == 8) {
                    $order = 'unverified';
                }
    
                $data = $this->LahanModel->getVerificationReport($startDate, $endDate, $offset, $limit, $sVal, $order, $dir, $type, 0);
                $totalResult = $this->LahanModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadVerifikasiPerLahanReport($startDate, $endDate, $type = 0) {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');
            
            if(stristr($session['select_db'], 'kwlm') === FALSE) {
                $order = 'u.name';
            } else {
                $order = 'u.id';
            }
            $data = $this->LahanModel->getVerificationReport($startDate, $endDate, 0, 0, '', $order, 'asc', $type);
            $total_pohon = 0;
            $total_verified_valid = 0;
            $total_verified_invalid = 0;
            $total_unverified = 0;
            foreach ($data as $item){
                $total_pohon += $item->assigned;
                $total_verified_valid += $item->verified_valid;
                $total_verified_invalid += $item->verified_invalid;
                $total_unverified += $item->unverified;
            }
            
            if($startDate == '0000-00-00' AND $endDate == '0000-00-00'){
                $range_date = "ALL TIME";
            } else{
                $range_date = date_format(date_create($startDate), "d-m-Y")." - ". date_format(date_create($endDate), "d-m-Y");
            }
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm_v'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) sebelum 2017";
                $file_name = "Laporan Verifikasi Per Lahan KWLM sebelum 2017 ".date("d-m-Y").".xlsx";
            } else if ($session['select_db'] == 'kayulestari_kwlm_prod'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM) 2017";
                $file_name = "Laporan Verifikasi Per Lahan KWLM 2017 ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Verifikasi Per Lahan KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Verifikasi per Lahan");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:I2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:C3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:C4");
            $objPHPExcel->getActiveSheet()->mergeCells("A5:C5");
            $objPHPExcel->getActiveSheet()->mergeCells("A6:C6");
            $objPHPExcel->getActiveSheet()->mergeCells("A7:C7");
            $objPHPExcel->getActiveSheet()->mergeCells("A8:C8");
            $objPHPExcel->getActiveSheet()->mergeCells("D3:I3");
            $objPHPExcel->getActiveSheet()->mergeCells("D4:I4");
            $objPHPExcel->getActiveSheet()->mergeCells("D5:I5");
            $objPHPExcel->getActiveSheet()->mergeCells("D6:I6");
            $objPHPExcel->getActiveSheet()->mergeCells("D7:I7");
            $objPHPExcel->getActiveSheet()->mergeCells("D8:I8");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Range Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D4', $range_date);
            $objPHPExcel->getActiveSheet()->getStyle('D3:D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Jumlah Assigned');
            $objPHPExcel->getActiveSheet()->setCellValue('D5', $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Jumlah Verified(Valid)');
            $objPHPExcel->getActiveSheet()->setCellValue('D6', $total_verified_valid);
            $objPHPExcel->getActiveSheet()->setCellValue('A7', 'Jumlah Verified(Invalid)');
            $objPHPExcel->getActiveSheet()->setCellValue('D7', $total_verified_invalid);
            $objPHPExcel->getActiveSheet()->setCellValue('A8', 'Jumlah Unverified');
            $objPHPExcel->getActiveSheet()->setCellValue('D8', $total_unverified);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:I8')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 10;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Pendata');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'No Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Anggota');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Nama Lahan');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Assigned');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Verified(Valid)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Verified(Invalid)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Unverified');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->pendata);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->no_anggota);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama);
                    $numberFormatBefore = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->nama_lahan);
                    $numberFormatStart = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->assigned);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->verified_valid);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->verified_invalid);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->unverified);

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("C".$ii.":".$numberFormatBefore.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($numberFormatStart.$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRow, $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('G'.$lastRow, $total_verified_valid);
            $objPHPExcel->getActiveSheet()->setCellValue('H'.$lastRow, $total_verified_invalid);
            $objPHPExcel->getActiveSheet()->setCellValue('I'.$lastRow, $total_unverified);
            $objPHPExcel->getActiveSheet()->getStyle('E'.$lastRow.":".'I'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$lastRow.":".'I'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php/transaction");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php/transaction");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
  
    public function kubikasiReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('report/js/kubikasi-report-js', $data, true);
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('report/kubikasiReport', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('dashboard', 'refresh');
            }
        } else {
            redirect('auth/login', 'refresh');
        }
    }
    
    public function ajaxKubikasiReportDataTable() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {

                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];
                
                $order = "jenis_pohon, kelas_diameter";
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
                if ($reqOrder[0]['column'] == 1) {
                    $order = 'jenis_pohon';
                } else if ($reqOrder[0]['column'] == 2) {
                    $order = 'kelas_diameter';
                } else if ($reqOrder[0]['column'] == 3) {
                    $order = 'jumlah_pohon';
                } else if ($reqOrder[0]['column'] == 4) {
                    $order = 'total_volume';
                } else if ($reqOrder[0]['column'] == 5) {
                    $order = 'average_volume';
                } 
    
                $data = $this->PohonModel->getKubikasiReport($offset, $limit, $sVal, $order, $dir);
                $totalResult = $this->PohonModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
 
    function downloadKubikasiReport() {
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
            $this->load->library('excel');
            
            $data = $this->PohonModel->getKubikasiReport(0, 0, '', "jenis_pohon, kelas_diameter", 'asc');
            $total_pohon = 0;
            $total_volume = 0;
            $total_average = 0;
            foreach ($data as $item){
                $total_pohon += $item->jumlah_pohon;
                $total_volume += $item->total_volume;
                $total_average += $item->average_volume;
            }
            
            $objPHPExcel = new PHPExcel();
            $objPHPExcel->setActiveSheetIndex(0);
            if ($session['select_db'] == 'kwlm'){
                $title = "Koperasi Wana Lestari Menoreh (KWLM)";
                $file_name = "Laporan Kubikasi KWLM ".date("d-m-Y").".xlsx";
            } else {
                $title = "Koperasi Hutan Jaya Lestari (KHJL)";
                $file_name = "Laporan Kubikasi KHJL ".date("d-m-Y").".xlsx";
            }

            $objPHPExcel->getActiveSheet()->setTitle("Laporan Kubikasi");
                       
            $objPHPExcel->getActiveSheet()->mergeCells("A1:F2");
            $objPHPExcel->getActiveSheet()->mergeCells("A3:C3");
            $objPHPExcel->getActiveSheet()->mergeCells("A4:C4");
            $objPHPExcel->getActiveSheet()->mergeCells("A5:C5");
            $objPHPExcel->getActiveSheet()->mergeCells("A6:C6");
            $objPHPExcel->getActiveSheet()->mergeCells("D3:F3");
            $objPHPExcel->getActiveSheet()->mergeCells("D4:F4");
            $objPHPExcel->getActiveSheet()->mergeCells("D5:F5");
            $objPHPExcel->getActiveSheet()->mergeCells("D6:F6");
            
            $objPHPExcel->getActiveSheet()->setCellValue('A1', $title);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $objPHPExcel->getActiveSheet()->setCellValue('A3', 'Printed Date');
            $objPHPExcel->getActiveSheet()->setCellValue('D3', date("d-m-Y h:i:sa"));
            $objPHPExcel->getActiveSheet()->getStyle('D3:D7')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->setCellValue('A4', 'Jumlah Pohon');
            $objPHPExcel->getActiveSheet()->setCellValue('D4', $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('A5', 'Total Volume');
            $objPHPExcel->getActiveSheet()->setCellValue('D5', $total_volume);
            $objPHPExcel->getActiveSheet()->setCellValue('A6', 'Average Volume');
            $objPHPExcel->getActiveSheet()->setCellValue('D6', $total_average);
            
            $objPHPExcel->getActiveSheet()->getStyle("A1")->getFont()->setSize(14);
            $objPHPExcel->getActiveSheet()->getStyle('A1:F6')->getFont()->setBold(true);
            
            $currentHeaderColumn = 'A';
            $currentRow = 8;
            
            $objPHPExcel->getActiveSheet()->setCellValue($currentHeaderColumn.$currentRow, 'No');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jenis Pohon');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Kelas Diameter (cm)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Jumlah Pohon');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Total Volume (m3)');
            $objPHPExcel->getActiveSheet()->setCellValue((++$currentHeaderColumn).$currentRow, 'Average Volume (m3)');
            $lastColumn = $currentHeaderColumn;

            $lastColumnIndex = ''.++$currentHeaderColumn;
            for($col='A'; $col != $lastColumnIndex; $col++) {
                $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                $objPHPExcel->getActiveSheet()->getStyle($col.$currentRow)->getFont()->setBold(true);
            }

            $ii = $currentRow + 1;
            if($data){
                foreach ($data as $item) {
                    $currentColumnValueIndex = 'A';
                    $objPHPExcel->getActiveSheet()->setCellValue($currentColumnValueIndex.$ii, $ii-$currentRow);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jenis_pohon);
                    $numberFormatBefore = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->kelas_diameter);
                    $numberFormatStart = $currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->jumlah_pohon);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->total_volume);
                    $objPHPExcel->getActiveSheet()->setCellValue((++$currentColumnValueIndex).$ii, $item->average_volume);

                    $lastColumnValueIndex = ++$currentColumnValueIndex;
                    $objPHPExcel->getActiveSheet()->getStyle("B".$ii.":".$numberFormatBefore.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
                    $objPHPExcel->getActiveSheet()->getStyle($numberFormatStart.$ii.":".$lastColumnValueIndex.$ii)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
                    $ii++;
                }
            }
              
            $lastRow = $ii++;
            $objPHPExcel->getActiveSheet()->setCellValue('B'.$lastRow, 'Total');
            $objPHPExcel->getActiveSheet()->setCellValue('D'.$lastRow, $total_pohon);
            $objPHPExcel->getActiveSheet()->setCellValue('E'.$lastRow, $total_volume);
            $objPHPExcel->getActiveSheet()->setCellValue('F'.$lastRow, $total_average);
            $objPHPExcel->getActiveSheet()->getStyle('D'.$lastRow.":".'F'.$lastRow)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
            $objPHPExcel->getActiveSheet()->getStyle('B'.$lastRow.":".'F'.$lastRow)->getFont()->setBold(true);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');

            header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
            header("Cache-Control: no-store, no-cache, must-revalidate");
            header("Cache-Control: post-check=0, pre-check=0", false);
            header("Pragma: no-cache");
            header("Writer      : Excel2007 (PHPExcel_Writer_Excel2007)");
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            
            header('Content-Disposition: attachment;filename="'.$file_name);
            
            $objWriter->save("php://output");

            }  else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                redirect(base_url() . "index.php");
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            redirect(base_url() . "index.php");
        }
    }

//  ====================================================================================================================================
//  ====================================================================================================================================
       
}
?>