<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();
/**
* 
*/
class Dashboard extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
        
		$this->load->model('PohonModel', '', TRUE);
        
        if ($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
        }
    }

    public function index(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('js-include/dashboard-js', $data, true);
//                $data['verif'] = $this->UserModel->findAllVerif();
                $data['startDate'] = date('Y-m-d');
                $data['endDate'] = date('Y-m-d');
                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('dashboard', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('auth/login', 'refresh');
            }           
        } else {
            redirect('auth/login', 'refresh');
        }
    }

    public function analysis(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $daterange = date('Y-m-d',strtotime("-1 days"))." - ".date('Y-m-d',strtotime("-1 days"));
                if($this->input->post('daterangepicker') != ''){
                    $daterange = $this->input->post('daterangepicker');
                }

                $daterangepicker = explode(' - ',$daterange);
                $startDate = date('Y-m-d', strtotime($daterangepicker[0]));
                $endDate = date('Y-m-d', strtotime($daterangepicker[1]));
                
                $data['pendataan'] = $this->PohonModel->getPendataanForDashboard($startDate, $endDate);
//                $data['verifikasi'] = $this->PohonModel->getVerificationForDashboard($startDate, $endDate);
                $total_pohon = 0;
                $total_lahan = 0;
                foreach ($data['pendataan'] as $pendataan) {
                    $total_pohon += $pendataan->jumlah_pohon;
                    $total_lahan += $pendataan->jumlah_lahan;
                }           
                $data['total_pohon'] = $total_pohon;
                $data['total_lahan'] = $total_lahan;
                header("Content-Type: application/json");
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';
            
            if($print){
                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            } else{
                return $data;
            }
        }
    }
    
    function sortingTopItem($allMenu = array()){
        $sortedByCount = array();
        if($allMenu){
            foreach($allMenu as $menu){
                if($menu->count != NULL && $menu->count != 0){
                    if(isset($sortedByCount[$menu->count])){
                        array_push($sortedByCount[$menu->count], $menu);
                    }
                    else{
                        $sortedByCount[$menu->count] = array();
                        array_push($sortedByCount[$menu->count], $menu);
                    }
                }
            }
        }
        krsort($sortedByCount);
        $finalMenuList = array();
        if($sortedByCount){
            foreach($sortedByCount as $count => $menuListByCount){
                if($menuListByCount){
                    $sortedByValue = array();
                    $menuById = array();
                    foreach($menuListByCount as $menu){
                        $sortedByValue[$menu->menu_id] = $menu->total_price;
                        $menuById[$menu->menu_id] = $menu;
                    }
                    arsort($sortedByValue);
                    foreach($sortedByValue as $menu_id => $total_price){
                        array_push($finalMenuList, $menuById[$menu_id]);
                    }
                }
            }
        }
        return $finalMenuList;
    }
}

?>