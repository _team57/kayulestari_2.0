<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
session_start();

	/**
	* @dzafiars
	*/
class Lahan extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('LahanModel', '', TRUE);
        $this->load->helper(array('form', 'url'));
        $this->load->library('session');

        if ($this->session->userdata('logged_in')) {
            $session = $this->session->userdata('logged_in');
        }
    }

    public function index(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $data['title'] = $session['select_db'];
                $data['useDatatable'] = true;
                $data['extendFooter'] = $this->load->view('js-include/lahanList-js', $data, true);

                $data['useDatatable'] = true;

                $this->load->view('include/header', $data);
                $this->load->view('include/nav', $data);
                $this->load->view('lahanList', $data);
                $this->load->view('include/footer', $data);
            } else {
                redirect('user', 'refresh');
            }    		
        } else {
            redirect('auth', 'refresh');
        }
    }
 
    public function ajaxLahanDataTable(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                $offset = $this->input->get('start');
                $limit = $this->input->get('length');
                $draw = $this->input->get('draw');
                $search = $this->input->get('search');
                $sVal = $search['value'];

                $order = 'anggota.nama';
                $dir = 'asc';
                $reqOrder = $this->input->get('order');
                if (isset($reqOrder[0]['dir'])) {
                    $dir = $reqOrder[0]['dir'];
                }
//                if ($reqOrder[0]['column'] == 1) {
//                    $order = 'u1.username';
//                } else if ($reqOrder[0]['column'] == 2) {
//                    $order = 'u1.name';
//                } else if ($reqOrder[0]['column'] == 3) {
//                    $order = 'u1.role_id';
//                } else if ($reqOrder[0]['column'] == 4) {
//                    $order = 'role';
//                } else if ($reqOrder[0]['column'] == 5) {
//                    $order = 'u1.spv_id';
//                } else if ($reqOrder[0]['column'] == 6) {
//                    $order = 'spv_name';
//                } 

                $data = $this->LahanModel->findAllWithPaging($offset, $limit, $sVal, $order, $dir);
                $totalResult = $this->LahanModel->getTotalResult();

                $dataTableArray = array();
                $dataTableArray['draw'] = $draw;
                $dataTableArray['recordsTotal'] = $totalResult;
                $dataTableArray['recordsFiltered'] = $totalResult;
                $dataTableArray['data'] = $data;

                header("Content-Type: application/json");
                echo json_encode($dataTableArray);
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
                echo json_encode($data);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
            echo json_encode($data);
        }
    }
   
    public function ajaxCreateUser(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $user = new UserModel();
                $user->username = $this->input->post('username');
                $user->name = $this->input->post('name');
                $password = "TOKEN:".$user->username.$this->input->post('password');
                $hashPassword = sha1($password);
                $user->password = $hashPassword;
                $user->role_id = $this->input->post('role_id');
                $user->spv_id = $this->input->post('spv_id');

                $result = $user->save();
                
                if($result){
                    $data['msg'] = 'User added';
                    $data['status'] = 'SUCCESS';
                }else{
                    $data['msg'] = 'Failed to save inventory';
                    $data['status'] = 'ERROR';
                }
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    function ajaxGetUser($id){
        if ($session = $this->session->userdata('logged_in')) {
            if ($this->input->post('id')) {
                if ($session['role_id']  > 0) {
                    header("Content-Type: application/json");
                    $user = $this->UserModel->findById($this->input->post('id'));

                    if ($user) {
                        $data['msg'] = 'User telah didapat.';
                        $data['status'] = 'SUCCESS';
                        $data['data'] = $user;
                    } else {
                        $data['msg'] = 'User tidak ditemukan di database.';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['status'] = 'ERROR';
                    $data['msg'] = 'Unauthorized';

                    header('HTTP/1.1 401 Unauthorized', true, 401);
                }
            } else {
                $data['msg'] = 'Parameter Kurang';
                $data['status'] = 'ERROR';
                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    public function ajaxUpdateUser(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $user = $this->UserModel->findById($this->input->post('id'));
                if ($user) {
                    $user = new UserModel();
                    $user->username = $this->input->post('username');
                    $password = "TOKEN:".$user->username.$this->input->post('password');
                    $hashPassword = sha1($password);
                    $user->password = $hashPassword;
                    $user->name = $this->input->post('name');
                    $user->role_id = $this->input->post('role_id');
                    $user->spv_id = $this->input->post('spv_id');
                    $user->id = $this->input->post('id');
                    
                    $result = $user->update();
                    
                    if ($result) {
                        $data['msg'] = 'User telah diupdate';
                        $data['status'] = 'SUCCESS';
                    } else {
                        $data['msg'] = 'User gagal diupdate';
                        $data['status'] = 'ERROR';
                    }
                } else {
                    $data['msg'] = 'User tidak ditemukan di database.';
                    $data['status'] = 'ERROR';
                }	            
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }
    
    public function ajaxDeleteUser(){
        if ($session = $this->session->userdata('logged_in')) {
            if ($session['role_id']  > 0) {
                header("Content-Type: application/json");
                $id = $this->input->post('id');
                $deleteStatus = $this->UserModel->deleteById($id);

                if ($deleteStatus) {
                    $data['msg'] = 'User telah dihapus.';
                    $data['status'] = 'SUCCESS';
                } else {
                    $data['msg'] = 'User gagal dihapus';
                    $data['status'] = 'ERROR';
                }
            } else {
                $data['status'] = 'ERROR';
                $data['msg'] = 'Unauthorized';

                header('HTTP/1.1 401 Unauthorized', true, 401);
            }
        } else {
            $data['status'] = 'ERROR';
            $data['msg'] = 'Unauthorized';

            header('HTTP/1.1 401 Unauthorized', true, 401);
        }
        echo json_encode($data);
    }

}
?>