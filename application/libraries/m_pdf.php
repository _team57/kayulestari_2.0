<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
 
include_once APPPATH.'/third_party/mpdf/mpdf.php';
class m_pdf {
    
    
    public $param;
    public $pdf;

 
    public function __construct($param='"en-GB-x","Letter",0,"",12.7, 12.7, 14, 12.7, 8, 8')
    {
        $this->param = $param;
        $this->pdf = new mPDF($this->param);
    }

    public function reloadMpdf($param=''){
    	if($param == ''){
    		$param = $this->param;
    	}
    	$this->pdf = new mPDF($param);
    }
}