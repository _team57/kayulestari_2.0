/* * ******************************************
  Goarchipelago Database Login for CodeIgniter
  to deliver login authentification for REST_Controller
  on https://github.com/chriskacerguis/codeigniter-restserver
  ---------------------------------------------
  by Muhamad Fajar

  How to install and use
  ----------------------
  I will write it later


 * ****************************************** */
<?php if( ! defined('BASEPATH')) exit('No direct script access allowed');
class DB_Login{
    function __construct(){
        $CI =& get_instance();
        $CI->load->library('session');
        $CI->load->model('UserModel');
        $CI->load->config('rest');
    }
    public function validation($email, $password){
        $user = new UserModel();
        $user->email = $email;
        $user->password = md5($password);
        $authUser = $user->login();

        if ($authUser) {
            $authUser->lastActive = date('Y-m-d H:i:s');
            $authUser->update();

            $sessArray = array(
                'email' => $authUser->email,
                'userId' => $authUser->userId,
                'roleId' => $authUser->roleId,
                'firstName' => $authUser->firstName,
                'lastName' => $authUser->lastName,
                'profilePicture' => $authUser->profilePicture,
                'newMessage' => $authUser->newMessage,
                'newNotif' => $authUser->newNotif,
                'verified' => $authUser->emailValidation
            );

            $CI->session->set_userdata('logged_in', $sessArray);
            
            return md5($email.':'.$CI->config->item('rest_realm').':'.$password);
        } else {
            
            return FALSE;
        }
    }
}