<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @dzafiars
 */
class PohonModel Extends CI_Model {

    const TABLE_NAME = 'pohon';

    var $totalResult = 0;
    var $resultIndex = 1; //start from 1
    
    var $id = '';
    var $uid = '';
    var $lahan_id = '';
    var $no_pohon = '';
    var $foto = '';
	var $jenis_pohon = '';
	var $keliling = '';
	var $tinggi = '';
	var $bentuk_batang = '';
	var $kondisi = '';
	var $lat = '';
	var $lng = '';
	var $timestamp = '';
	var $user_id = '';
	var $is_valid = '';
	var $vstatus = '';
	var $vdate = '';
	var $verificator = '';
	var $is_tebang = '';
	var $status = '';
	var $keliling_manual = '';
	var $app_source = '';

    function __construct() {
        parent::__construct();
        $this->load->database();
        
//        $tempArray = array(
//            'kelurahan' => 0.0,
//            'kecamatan' => 0,
//            'kabupaten/kota' => false,
//            'provinsi' => 0,
//            'no_ktp' => 0,
//            'alamat' => 0,
//            'telepon' => 0,
//            'hp' => 0,
//            'pekerjaan' => 0,
//        );
//
//        $this->extra = (object) $tempArray;
        $this->load->model('DatabaseModel', '', TRUE);
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->load->database($configDB, TRUE);
        }
    }

    public function setDB($database = ''){
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->db = $this->load->database($configDB, TRUE);
        }
    }
 
    public function findAll() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->from(self::TABLE_NAME);
        $query = $this->db->get();
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
 
    public function findAllWithPaging($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select("SQL_CALC_FOUND_ROWS 
            anggota.nama AS nama_anggota, 
            lahan.nama_lahan, 
            no_pohon, 
            IFNULL(pohon.foto, 'Tidak Ada') as foto,
            jenis_pohon, 
            keliling,
            keliling / 3.14 as diameter,
            bentuk_batang,
            kondisi", false
        );
        
        $this->db->join("lahan", "lahan.id = pohon.lahan_id");
        $this->db->join("anggota", "anggota.no_anggota = lahan.no_anggota");
        $this->db->where("pohon.is_valid", 1);
        if ($sVal != '') {
            $this->db->where('('.
                'anggota.nama LIKE "%'.$sVal.'%" OR '.
                'nama_lahan LIKE "%'.$sVal.'%" OR '.
                'no_pohon LIKE "%'.$sVal.'%" OR '.
                'jenis_pohon LIKE "%'.$sVal.'%" OR '.
                'keliling LIKE "%'.$sVal.'%" OR '.
                'bentuk_batang LIKE "%'.$sVal.'%" OR '.
                'kondisi LIKE "%'.$sVal.'%"'.
            ')');
        }
        $this->db->order_by($order, $dir);
        
        if($limit > 0){
            $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
        } else {
            $query = $this->db->get(self::TABLE_NAME);
        }
                
        $result = $query->result();
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        return $this->mapMultipleResult($result, $offset + 1);
    }
    
    public function getVerificationReport($startDate, $endDate, $offset, $limit, $sVal, $order, $dir, $type) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        
//        if ($type == 0){
            if(stristr($session['select_db'], 'kwlm') === FALSE) {
                $userRule = "u.id BETWEEN 124 AND 139";
            } else{
                $userRule = "u.id BETWEEN 79 AND 98";
            }
//        } else if($type == 1){
//            $userRule = "u.id BETWEEN 79 AND 83";
//        } else if($type == 2){
//            $userRule = "u.id BETWEEN 84 AND 88";
//        }
        
        if($startDate != '0000-00-00' AND $endDate != '0000-00-00'){
            $timestampRule = " AND pohon.timestamp BETWEEN '".$startDate." 00:00:00' AND '".$endDate." 23:59:59'";
            if($startDate == '2017-01-01' AND $endDate == '2017-01-13'){
                $userRule = "u.id BETWEEN 79 AND 83";
            } elseif ($startDate == '2017-01-16' AND $endDate == '2017-01-31') {
                $userRule = "u.id BETWEEN 84 AND 88";
            } elseif ($startDate == '2017-02-01' AND $endDate == '2017-02-15') {
                $userRule = "u.id BETWEEN 89 AND 93";
            }
        } else {
            $timestampRule = " ";
        }
        if ($limit > 0){
            $limitRule = " LIMIT ".$limit." OFFSET ".$offset;
        } else {
            $limitRule = "";
        }
        if(stristr($session['select_db'], 'kwlm') === FALSE) {
            $selectRule = "
                IFNULL(COUNT(pohon.id), 0) as verified,
                IFNULL(p.assigned - COUNT(pohon.id), 0) AS unverified
                ";
            $rule = "
                INNER JOIN user_lahan ul ON ul.user_id = p.user_id
                INNER JOIN lahan l ON l.id = ul.lahan_id
                INNER JOIN pohon ON pohon.lahan_id = l.id
                WHERE pohon.is_valid = 1 AND keliling IS NOT NULL AND keliling != 0".$timestampRule;
        } else{
            $selectRule = "
                IFNULL(p3.valid, 0) AS verified_valid,
                IFNULL(p2.invalid, 0) AS verified_invalid,
                IFNULL(p.assigned - (IFNULL(p3.valid, 0) + IFNULL(p2.invalid, 0)), 0) AS unverified";
            $rule = "
                LEFT JOIN (
                    SELECT
                        COUNT(pohon.id) as invalid,
                        verificator
                    FROM
                        pohon
                    WHERE
                        keliling = 0
                    AND verificator IS NOT NULL
                    AND is_valid = 1".$timestampRule."
                    GROUP BY verificator
                ) AS p2 ON p.user_id = p2.verificator
                LEFT JOIN (
                    SELECT
                        COUNT(pohon.id) AS valid,
                        verificator
                    FROM
                        pohon
                    WHERE
                        keliling != 0
                    AND verificator IS NOT NULL
                    AND is_valid = 1".$timestampRule."
                    GROUP BY
                        verificator
                ) AS p3 ON p.user_id = p3.verificator";
        }
        $query = "
            SELECT
                p.nama_user,
                p.username,
                p.jumlah_lahan,
                p.assigned,".$selectRule."
            FROM
                (
                    SELECT
                        u.id AS user_id,
                        u.`name` AS nama_user,
                        u.username,
                        COUNT(pohon.id) AS assigned,
                        COUNT(DISTINCT l.id) as jumlah_lahan
                    FROM
                        `user` u
                    INNER JOIN user_lahan ul ON ul.user_id = u.id
                    INNER JOIN lahan l ON l.id = ul.lahan_id
                    INNER JOIN pohon ON pohon.lahan_id = l.id
                    WHERE ".$userRule.$timestampRule."
                    AND pohon.is_valid = 1
                    GROUP BY
                        u.`name`
                    ORDER BY ".$order." ".$dir.
                    $limitRule."
                ) AS p".$rule."
            GROUP BY p.user_id";
        
        $result = $this->db->query($query)->result();
//        echo $this->db->last_query();exit;
        $queryTotal = $this->db->query("
            SELECT
                count(DISTINCT u.id) as count
            FROM
                `user` u
            INNER JOIN user_lahan ul ON ul.user_id = u.id
            INNER JOIN lahan l ON l.id = ul.lahan_id
            INNER JOIN pohon ON pohon.lahan_id = l.id
            WHERE ".$userRule.$timestampRule."
            AND pohon.is_valid = 1"
        );
        $totalResult = $queryTotal->result();
        $this->totalResult = $totalResult[0]->count;

        $data = $this->mapMultipleResult($result, $offset + 1);
        return $data;
    }
     
    public function getPendataanReport($start, $end, $user_id, $offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        if ($start AND $end){
            $timestampRule = " AND `timestamp` BETWEEN '".$start." 00:00:00' AND '".$end." 23:59:59'"; 
        } else {
            $timestampRule = "";
        }
        if($user_id > 0){
            $userIdRule = " AND user.id = ".$user_id;
        } else {
            $userIdRule = "";
        }
        if ($limit > 0){
            $limitRule = " LIMIT ".$limit." OFFSET ".$offset;
        } else {
            $limitRule = "";
        }
        if ($sVal != ''){
            $searchRule = " AND (
                `user`.`name` LIKE '%".$sVal."%' OR 
                DATE_FORMAT(`timestamp`, '%e %M %Y') LIKE '%".$sVal."%' OR 
                anggota.no_anggota LIKE '%".$sVal."%' OR 
                anggota.nama LIKE '%".$sVal."%' OR 
                nama_lahan LIKE '%".$sVal."%')";
        } else {
            $searchRule = "";
        }
        
        $query = "
            SELECT
                `user`.`name` AS pendata,
                DATE_FORMAT(`timestamp`, '%e %M %Y') AS tanggal_pendataan,
                anggota.no_anggota AS no_anggota,
                anggota.nama AS nama_anggota,
                nama_lahan,
                min(`timestamp`) AS awal_pendataan,
                max(`timestamp`) AS akhir_pendataan,
                count(pohon.id) AS jumlah_pohon
            FROM
                pohon
            INNER JOIN lahan ON lahan.id = pohon.lahan_id
            INNER JOIN anggota ON anggota.no_anggota = lahan.no_anggota
            INNER JOIN `user` ON `user`.id = pohon.user_id
            WHERE pohon.is_valid = 1"
                .$timestampRule.$userIdRule.$searchRule."
            GROUP BY
                DATE_FORMAT(`timestamp`, '%e %M %Y'),
                `user`.`name`,
                anggota.no_anggota,
                lahan_id,
                nama_lahan
            ORDER BY ".$order." ".$dir.
            $limitRule
        ;
        
        $result = $this->db->query($query)->result();

        $queryTotal = $this->db->query("
            SELECT COUNT(1) AS `count`
            FROM
                pohon
            INNER JOIN lahan ON lahan.id = pohon.lahan_id
            INNER JOIN anggota ON anggota.no_anggota = lahan.no_anggota
            INNER JOIN `user` ON `user`.id = pohon.user_id
            WHERE
                pohon.is_valid = 1"
                .$timestampRule.$userIdRule.$searchRule."
            GROUP BY
                DATE_FORMAT(`timestamp`, '%e %M %Y'),
                `user`.`name`,
                anggota.no_anggota,
                lahan_id,
                nama_lahan"
        );
        
        $totalResult = $queryTotal->result();
        $this->totalResult = count($totalResult);

        $data = $this->mapMultipleResult($result, $offset + 1);
        return $data;
    }
     
    public function getPendataanForDashboard($start, $end, $user_id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        if ($start AND $end){
            $timestampRule = " AND `timestamp` BETWEEN '".$start." 00:00:00' AND '".$end." 23:59:59'"; 
        } else {
            $timestampRule = "";
        }
        if($user_id > 0){
            $userIdRule = " AND user.id = ".$user_id;
        } else {
            $userIdRule = "";
        }
        
        $query = "
            SELECT
                `user`.`name` AS pendata,
                DATE_FORMAT(`timestamp`, '%e %M %Y') AS tanggal_pendataan,
                anggota.no_anggota AS no_anggota,
                anggota.nama AS nama_anggota,
                nama_lahan,
                min(`timestamp`) AS awal_pendataan,
                max(`timestamp`) AS akhir_pendataan,
                count(pohon.id) AS jumlah_pohon,
                count(DISTINCT lahan.id) AS jumlah_lahan
            FROM
                pohon
            INNER JOIN lahan ON lahan.id = pohon.lahan_id
            INNER JOIN anggota ON anggota.no_anggota = lahan.no_anggota
            INNER JOIN `user` ON `user`.id = pohon.user_id
            WHERE pohon.is_valid = 1"
                .$timestampRule.$userIdRule."
            GROUP BY
                DATE_FORMAT(`timestamp`, '%e %M %Y'),
                `user`.`name`,
                anggota.no_anggota,
                lahan_id,
                nama_lahan"
        ;
        
        $result = $this->db->query($query)->result();
        
        $data = $this->mapMultipleResult($result);
        return $data;
    }
    
    public function getVerificationForDashboard($start, $end) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        if ($start AND $end){
            $timestampRule = " AND `timestamp` BETWEEN '".$start." 00:00:00' AND '".$end." 23:59:59'"; 
        } else {
            $timestampRule = "";
        }
        if($user_id > 0){
            $userIdRule = " AND user.id = ".$user_id;
        } else {
            $userIdRule = "";
        }
        
        $query = "
            SELECT
                `user`.`name` AS pendata,
                DATE_FORMAT(`timestamp`, '%e %M %Y') AS tanggal_pendataan,
                anggota.no_anggota AS no_anggota,
                anggota.nama AS nama_anggota,
                nama_lahan,
                min(`timestamp`) AS awal_pendataan,
                max(`timestamp`) AS akhir_pendataan,
                count(pohon.id) AS jumlah_pohon,
                count(DISTINCT lahan.id) AS jumlah_lahan
            FROM
                pohon
            INNER JOIN lahan ON lahan.id = pohon.lahan_id
            INNER JOIN anggota ON anggota.no_anggota = lahan.no_anggota
            INNER JOIN `user` ON `user`.id = pohon.user_id
            WHERE pohon.is_valid = 1"
                .$timestampRule.$userIdRule."
            GROUP BY
                DATE_FORMAT(`timestamp`, '%e %M %Y'),
                `user`.`name`,
                anggota.no_anggota,
                lahan_id,
                nama_lahan"
        ;
        
        $result = $this->db->query($query)->result();
        
        $data = $this->mapMultipleResult($result);
        return $data;
    }
        
    public function getKubikasiReport($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        
        if ($limit > 0){
            $limitRule = " LIMIT ".$limit." OFFSET ".$offset;
        } else {
            $limitRule = "";
        }
        
        if ($sVal != ''){
            $searchRule = " AND (
                jenis_pohon LIKE '%".$sVal."%')";
        } else {
            $searchRule = "";
        }
        
        $query = "
            SELECT SQL_CALC_FOUND_ROWS
                jenis_pohon,
                CASE
                    WHEN keliling / 3.14 BETWEEN 10
                    AND 15 THEN
                            '10 - 14'
                    WHEN keliling / 3.14 BETWEEN 15
                    AND 20 THEN
                            '15 - 19'
                    WHEN keliling / 3.14 BETWEEN 20
                    AND 30 THEN
                            '20 - 29'
                    WHEN keliling / 3.14 BETWEEN 30
                    AND 40 THEN
                            '30 - 39'
                    WHEN keliling / 3.14 BETWEEN 40
                    AND 50 THEN
                            '40 - 49'
                    WHEN keliling / 3.14 >= 50 THEN
                            '50 up'
                    ELSE
                            '< 10'
                END AS kelas_diameter,
                COUNT(id) AS jumlah_pohon,
                ROUND(
                        SUM(
                                0.6 * 3.14 * POWER(keliling / 3.14 / 2, 2) * tinggi
                        ) / 1000000,
                        2
                ) AS total_volume,
                 ROUND(
                        ROUND(
                                SUM(
                                        0.6 * 3.14 * POWER(keliling / 3.14 / 2, 2) * tinggi
                                ) / 1000000,
                                2
                        ) / COUNT(id),
                        2
                ) AS average_volume
            FROM
                pohon
            WHERE
                is_valid = 1
                AND keliling IS NOT NULL
                AND keliling != 0
                AND tinggi != 0".$searchRule."
            GROUP BY
                jenis_pohon,
                kelas_diameter
            ORDER BY ".$order." ".$dir.$limitRule
        ;
        
        $result = $this->db->query($query)->result();
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;

        $data = $this->mapMultipleResult($result, $offset+1);
//        return array_slice($data,$offset,$limit);
        return $data;
    }

    public function save() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = $this->getSaveData();
        $this->db->insert(self::TABLE_NAME, $data);

        return $this->db->insert_id();
    }

    public function update() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = array(
            'username' => $this->username,
            'password' => $this->password,
            'name' => $this->name,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );
        $this->db->where('id', $this->id);

        return $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function findById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            $data = $this->mapSingleResult($result);

            return $data;
        } else {
            return FALSE;
        }
    }

    public function deleteById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $query = $this->db->delete(self::TABLE_NAME, array('id' => $id));
        if($this->db->_error_message()){
            return false;
        }
        return $query; //return TRUE if Success, FALSE if failed
    }

    public function getTotalResult() {
        return $this->totalResult;
    }

    private function getData() {
        $data = array(
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            // 'profile_id' => $this->profile_id,
            'role_id' => $this->role_id,
            'profile' => json_encode($this->profile),
            'password' => $this->password
        );

        return $data;
    }

    private function getSaveData() {
        $data = array(
            'username' => $this->username,
            'name' => $this->name,
            'password' => $this->password,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );

        return $data;
    }

    private function mapSingleResult($result) {
        $data = new AnggotaModel();
        $data = $result[0];
        if($result[0]->profile){
            $data->profile = json_decode($result[0]->profile);
        }
        return $data;
    }

    private function mapMultipleResult($results, $index) {
        $arrayOfData = array();

        foreach ($results as $item) {
            $data = new PohonModel();
            $data = $item;
            $data->resultIndex = $index;$index++;
            array_push($arrayOfData, $data);
        }
        return $arrayOfData;
    }
    
    private function mapMultipleResultReport($results, $index) {
        $arrayOfData = array();
        $resultIndex = $index;
        foreach ($results as $item) {
            $data = new PohonModel();
            $data = $item;
            $data->resultIndex = $resultIndex;$resultIndex++;
            array_push($arrayOfData, $data);
        }
        
        return $arrayOfData;
    }
    
}

?>
