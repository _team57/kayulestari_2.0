<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

/**
 *
 */
class InventoryModel extends CI_Model {
	
	const TABLE_NAME = 'inventory';
	
	public $table_name = 'inventory';
	
	public $column_id = 'id';
	public $column_inventory_category_id = 'inventory_category_id';
	public $column_inventory_category_name = 'category_name';
	public $column_shop_id = 'shop_id';
	public $column_inventory_name = 'inventory_name';
	public $column_current_hr_qty = 'current_hr_qty';
	public $column_hr_unit = 'hr_unit';
	public $column_current_qty = 'current_qty';
	public $column_unit = 'unit';
	public $column_conversion = 'conversion';
	public $column_minimum_stock = 'minimum_stock';
	public $column_latest_restock = 'latest_restock';
	public $column_inventory_value = 'inventory_value';
	public $column_average_value = 'average_value';
	public $column_created_date = 'created_date';
	public $column_created_by = 'created_by';
	public $column_updated_date = 'updated_date';
	public $column_updated_by = 'updated_by';
	
	
	var $totalResult = 0;
	var $resultIndex = 1; //start from 1
	var $id = '';
	var $inventory_category_id = '';
	var $category_name = '';
	var $shop_id = '';
	var $inventory_name = '';
	var $current_hr_qty = '0';
	var $hr_unit = '';
	var $current_qty = '0';
	var $unit = '';
	var $conversion = '0';
	var $minimum_stock = '0';
	var $latest_restock = NULL;
	var $inventory_value = '0';
	var $average_value = '0';
	var $created_date = NULL;
	var $created_by = '';
	var $updated_date = NULL;
	var $updated_by = NULL;
        var $status_alert = 0;
	
        var $inventory_id = 0;
        
	function __construct($database = '') {
		parent::__construct();
		//$this->id = uniqid();
	
		$this->load->model('DatabaseModel', '', TRUE);
                $this->load->model('ShopModel', '', TRUE);
		if($database != ''){
			$configDB = $this->DatabaseModel->getConfigDB($database);
			$this->load->database($configDB, TRUE);
		}
		
		 if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
		}
	}
	
	public function setDB($database = ''){
		if($database != ''){
			$configDB = $this->DatabaseModel->getConfigDB($database);
			$this->db = $this->load->database($configDB, TRUE);
		}
	}
	
	public function findAll() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
		$this->db->select($this->table_name . '.*');
		$this->db->from($this->table_name);
		$this->db->order_by($this->column_id);
		$query = $this->db->get();
	
		return $this->mapMultipleResult($query->result());
	}
	
	public function findAllFilterByShopId() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
		$profileId = $this->session->userdata['logged_in']['userProfileId'];
        $this->load->model('ShopModel', '', TRUE);
        $arrayOfShopId = $this->ShopModel->getArrayOfShopIdOnProfileId($profileId);
		
		$this->db->select('inventory.*,
							shop.name as shopName
                            ');
        $this->db->from(self::TABLE_NAME);
		$this->db->join('shop', 'shop.id = inventory.shop_id');
        $this->db->where_in('inventory.shop_id', $arrayOfShopId);
        $this->db->order_by('id');
        $query = $this->db->get();
		
        return $this->mapMultipleResult($query->result());
	}
    
    public function findAllWithPaging($offset, $limit, $search = '', $order = 'id', $dir = 'desc', $shop_filter_id = 0, $po_id = 0) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->select('inventory.*');
		$this->db->select('inventory_category.category_name');
        $this->db->select('shop.name as shopName');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id'); 
        $this->db->where('inventory.is_active', 1);
        if ($search != '') {
            $this->db->where('(inventory_name LIKE "%' . $search . '%")');
        }
       if ($shop_filter_id > 0) {
            $this->db->where('shop.id', $shop_filter_id);
        }
        $this->db->order_by($order, $dir);
        $query = $this->db->get(self::TABLE_NAME, $limit, $offset);

        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        $results = $query->result();
        $data = $this->mapMultipleResult($results, $offset + 1);

        return $data;
    }
    
    public function findAllForExport($shop_filter_id = 0) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->select('inventory.*');
        $this->db->select('inventory_category.category_name');
        $this->db->select('shop.name as shopName');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id'); 
        $this->db->where('inventory.is_active', 1);
       if ($shop_filter_id > 0) {
            $this->db->where('shop.id', $shop_filter_id);
        }
        $query = $this->db->get(self::TABLE_NAME);
        
        $results = $query->result();
        $data = $this->mapMultipleResult($results);

        return $data;
    }
	
	public function findForReportWithPaging($offset, $limit, $search = '', $order = 'inventory.inventory_name', $dir = 'desc', $shop_filter_id = 0, $isDownload){
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS inventory.inventory_name', false);
        $this->db->select('sum(current_hr_qty) as total_hr_qty, hr_unit, sum(current_qty) as total_qty, unit, shop_id');
        
        if ($shop_filter_id > 0) {
            $this->db->where('shop_id', $shop_filter_id);
        }

        $this->db->where('inventory.is_active', 1);
        
        $this->db->group_by('inventory.inventory_name, shop_id');
        $this->db->order_by('inventory.inventory_name', $dir);
        
        if ($isDownload == true) {
            $query = $this->db->get(self::TABLE_NAME);
        }else
        {
            $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
        }
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        $results = $query->result();
        $data = $this->mapMultipleResult($results, $offset + 1, 0);
        
        return $data;
    }

    public function getTotalResultPaging($offset, $limit, $search = '', $order = 'id', $dir = 'desc', $shop_filter_id = 0, $po_id = 0){
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->select('inventory.*');
        $this->db->select('inventory_category.category_name');
        $this->db->select('shop.name as shopName');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id'); 
        $this->db->where('inventory.is_active', 1);
        if ($shop_filter_id > 0) {
            $this->db->where('shop.id', $shop_filter_id);
        }
        $this->db->order_by($order, $dir);
        $query = $this->db->get(self::TABLE_NAME);

        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        $results = $query->result();
        $data = $this->mapMultipleResult($results, $offset + 1);

        return count($data);
    }
    
    public function findAllForPO($po_id = 0) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('SQL_CALC_FOUND_ROWS *', false);
        $this->db->select('inventory.*');
        $this->db->select('inventory_category.category_name');
        $this->db->select('shop.name as shopName');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id'); 
        $this->db->where('inventory.is_active', 1);
        $query = $this->db->get(self::TABLE_NAME);
        
        $results = $query->result();
        $data = $this->mapMultipleResult($results, $offset + 1);

        return $data;
    }

	public function findStockMoving($offset, $limit, $search = '', $order = 'datetime', $dir = 'asc', $shop_filter_id = 0, $isDownload, $rule, $useCloseTime, $start, $end) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $arrayOfShopId = array();
        if ($this->session->userdata['logged_in']['userRoleId'] != 1) {
            $this->ShopUserModel->setDB("zeepos_client_" . $this->session->userdata['logged_in']['userProfileId']);
            $arrayOfShopId = $this->ShopUserModel->getArrayOfShopIdOnShopUserId($this->session->userdata['logged_in']['userId']);
        }
    
        $this->db->select('SQL_CALC_FOUND_ROWS *' , false);
		
        if (count($rule) > 0) {
            $this->db->where($rule);
        }
        
        if($search != ''){
            $this->db->where('(category_name LIKE "%' . $search . '%" OR inventory_name LIKE "%' . $search . '%" OR qty LIKE "%' . $search . '%" OR unit LIKE "%' . $search . '%" OR price LIKE "%' . $search . '%" OR total LIKE "%' . $search . '%" OR datetime LIKE "%' . $search . '%" OR transaction_type LIKE "%' . $search . '%")');
        }
		
		
        if (!$isDownload) {
            $this->db->order_by($order, $dir);
        } else {
            $this->db->order_by('inventorymoving.datetime', 'asc');
        }

        if (!$isDownload) {
            $query = $this->db->get('inventorymoving', $limit, $offset);
        } else {
            $query = $this->db->get('inventorymoving');
        }
		
		
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
    
        $results = $query->result();
        
        $data = $this->mapMultipleResult($results, $offset + 1);
        
        return $data;
		
	}
	    
	public function findInventoryMoving($shop_filter_id = 0, $rule, $useCloseTime, $start, $end) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $arrayOfShopId = array();
        if ($this->session->userdata['logged_in']['userRoleId'] != 1) {
            $this->ShopUserModel->setDB("zeepos_client_" . $this->session->userdata['logged_in']['userProfileId']);
            $arrayOfShopId = $this->ShopUserModel->getArrayOfShopIdOnShopUserId($this->session->userdata['logged_in']['userId']);
        }
        
        if($shop_filter_id != 0){
            $where_shop_incoming = " AND stock_incoming.shop_id = '".$shop_filter_id."'";
            $where_shop_transfer = " AND stock_transfer.host_shop_id = '".$shop_filter_id."'";
            $where_shop_waste = " AND inventory.shop_id = '".$shop_filter_id."'";
        }
        
        $query = $this->db->query("
            SELECT
                shop.`name` AS shop_name,
                stock_incoming.shop_id AS shop_id,
                stock_incoming.incoming_number AS reference_no,
                stock_incoming.incoming_date AS datetime,
                inventory_category.category_name AS inv_cat,
                inventory.inventory_name AS inv_name,
                stock_incoming_detail.hr_qty AS hr_qty_in,
                '0' AS hr_qty_out,
                inventory.unit AS hr_unit,
                'Masuk' AS category_type,
                'Stock Incoming' AS type,
                '' AS notes
            FROM
                stock_incoming
            JOIN stock_incoming_detail ON stock_incoming.id = stock_incoming_detail.stock_incoming_id
            JOIN inventory ON stock_incoming_detail.inventory_item_id = inventory.id
            JOIN inventory_category ON inventory_category.id = inventory.inventory_category_id
            JOIN shop ON shop.id = stock_incoming.shop_id
            WHERE stock_incoming.incoming_date >= '".$rule['datetime >=']."' AND stock_incoming.incoming_date <= '".$rule['datetime <=']."'".$where_shop_incoming."
            UNION
                SELECT
                    shop.`name` AS shop_name,
                    stock_transfer.host_shop_id AS shop_id,
                    stock_transfer.stock_transfer_number AS reference_no,
                    stock_transfer.transfer_date AS datetime,
                    inventory_category.category_name AS inv_cat,
                    inventory.inventory_name AS inv_name,
                    '0' AS hr_qty_in,
                    stock_transfer_detail.hr_qty AS hr_qty_out,
                    inventory.unit AS hr_unit,
                    'Keluar' AS category_type,
                    'Stock Transfer' AS type,
                    '' AS notes
                FROM
                    stock_transfer
                JOIN stock_transfer_detail ON stock_transfer.id = stock_transfer_detail.stock_transfer_id
                JOIN inventory ON stock_transfer_detail.inventory_item_id = inventory.id
                JOIN inventory_category ON inventory_category.id = inventory.inventory_category_id
                JOIN shop ON shop.id = stock_transfer.host_shop_id
                WHERE stock_transfer.transfer_date >= '".$rule['datetime >=']."' AND stock_transfer.transfer_date <= '".$rule['datetime <=']."'".$where_shop_transfer."
            UNION
                SELECT
                    shop.`name` AS shop_name,
                    inventory.shop_id as shop_id,
                    '' AS reference_no,
                    inventory_waste.waste_date AS datetime,
                    inventory_category.category_name AS inv_cat,
                    inventory.inventory_name AS inv_name,
                    '0' AS hr_qty_in,
                    inventory_waste.hr_qty AS hr_qty_out,
                    inventory.unit AS hr_unit,
                    'Keluar' AS category_type,
                    'Stock Waste' AS type,
                    inventory_waste.waste_reason AS notes
                FROM
                    inventory_waste
                JOIN inventory ON inventory_waste.inventory_item_id = inventory.id
                JOIN inventory_category ON inventory_category.id = inventory.inventory_category_id
                JOIN shop ON shop.id = inventory.shop_id
                WHERE inventory_waste.waste_date >= '".$rule['datetime >=']."' AND inventory_waste.waste_date <= '".$rule['datetime <=']."'".$where_shop_waste
        );
		
        $results = $query->result();
//        echo "<pre>";
//        print_r($results);
//        echo "</pre>";exit;
        $data = $this->mapMultipleResult($results, '', $useCloseTime, $start, $end);
        
        return $data;
		
	}
	  
    public function findAll2($offset, $limit, $search = '', $order = 'id', 
            $dir = 'desc', $rule = array(), $vendor_id = 0, $shop_filter_id = 0, $status = 1)
    {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $arrayOfShopId = array();
        if ($this->session->userdata['logged_in']['userRoleId'] != 1) {
            $this->ShopUserModel->setDB("zeepos_client_" . $this->session->userdata['logged_in']['userProfileId']);
            $arrayOfShopId = $this->ShopUserModel->getArrayOfShopIdOnShopUserId($this->session->userdata['logged_in']['userId']);
        }
                
        if ($vendor_id != 0) {
            $this->db->where($this->table_name . '.' . $this->column_vendor_id, $vendor_id);
        }
        if ($shop_filter_id != 0) {
            $this->db->where($this->table_name . '.shop_id', $shop_filter_id);
        }
        if ($this->session->userdata['logged_in']['userRoleId'] != 1) {
            $this->db->where_in('shop_id', $arrayOfShopId);
        }    
    
        $this->db->select('SQL_CALC_FOUND_ROWS inventory.*' , false);
        $this->db->select('inventory_category.category_name');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->where('(' 
        . 'inventory_category.' . $this->column_inventory_category_name . ' LIKE "%' . $search . '%" OR ' 
        . $this->table_name . '.' . $this->column_inventory_name . ' LIKE "%' . $search . '%" OR ' 
        . $this->table_name . '.' . $this->column_hr_unit .' LIKE "%' . $search . '%")');
    
        if (count($rule) > 0) {
            $this->db->where($rule);
        }
    
        $this->db->order_by($order, $dir);
        $query = $this->db->get($this->table_name, $limit, $offset);
            
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
    
        $results = $query->result();
        
        $data = $this->mapMultipleResult($results, $offset + 1);
        
        return $data;
    }

	function save() {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
		}
		$data = $this->getData();
        $this->db->insert(self::TABLE_NAME, $data);

        if ($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        } 
	}
        
        public function saveWithId() {
			if($this->session->userdata('logged_in')){
				$session = $this->session->userdata('logged_in');
				$this->setDB("zeepos_client_".$session['userProfileId']);
			}
            $data = $this->getData();
            $this->db->insert(self::TABLE_NAME, $data);

            return $this->db->insert_id();
        }
    
	function saveWaste() {
		   if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
		}
		$data = array(
            'inventory_item_id' => $this->inventory_item_id,
            'waste_date' => $this->waste_date,
            'hr_qty' => $this->hr_qty,
            'qty' => $this->qty,
            'waste_reason' => $this->waste_reason,
            'created_by' => $this->session->userdata['logged_in']['userProfileId'],
            'updated_by' => $this->updated_by
		);
        $this->db->insert('inventory_waste', $data);

        if ($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        } 
	}
    
    function setInactiveById($id){
        
        $data = array(
            'is_active' => 0
        );
        
        $this->db->where('id', $id);
        $this->db->update(self::TABLE_NAME, $data);
        
        if ($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }  
    }
	
	function bulkSave($arrayOf) {
		$arrayOfData = array();
		foreach ($arrayOf as $item) {
			$data = $item->getData();
			array_push($arrayOfData, $data);
		}
	
		return $this->db->insert_batch(self::TABLE_NAME, $arrayOfData);
	}
	
	function update() {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
		}
		$data = $this->getData();
		$this->db->where($this->column_id, $this->id);
        
		$this->db->update($this->table_name, $data);
         
        if ($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }  
	}
        
        function updateWithCustomDB($customProfile = '') {
            if ($customProfile == '') {
                $this->setDB('zeepos_client_' . $this->session->userdata['logged_in']['userProfileId']);
            } else {
                $this->setDB('zeepos_client_' . $customProfile);
            }
            $data = $this->getData();
            $this->db->where($this->column_id, $this->id);

            $this->db->update($this->table_name, $data);
         
            if ($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }  
	}
	
	public function findById($id){
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
		$this->db->select('*, inventory.id AS inventory_id, shop.id AS shop_id');
        $this->db->select('inventory_category.category_name');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->select('shop.name as shop_name');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id');
        $this->db->from(self::TABLE_NAME);
        $this->db->where($this->table_name.'.id', $id);
        $query = $this->db->get();
        
		if ($query->num_rows() == 1) {
			$result = $query->result();
			$data = $this->mapSingleResult($result);
	
			return $data;
		} else {
			return FALSE;
		}
	}
        
        public function findByIdRawWithCustomProfile($id, $customProfile = ''){
            if ($customProfile == '') {
                $this->setDB('zeepos_client_' . $this->session->userdata['logged_in']['userProfileId']);
            } else {
                $this->setDB('zeepos_client_' . $customProfile);
            }
            
            $this->db->select('*');
            $this->db->from(self::TABLE_NAME);
            $this->db->where($this->table_name.'.id', $id);
            $query = $this->db->get();

            if ($query->num_rows() == 1) {
                $result = $query->result();
                $data = $this->mapSingleResult($result);

                return $data;
            } else {
                return FALSE;
            }
        }
        
        public function findById2($id){
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
		$this->db->select('*, inventory.id AS inventory_id, shop.id AS shop_id');
        $this->db->select('inventory_category.category_name');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $this->db->select('shop.name as shop_name');
        $this->db->join('shop', 'shop.id = '.$this->table_name . '.shop_id');
        $this->db->from(self::TABLE_NAME);
        $this->db->where($this->table_name.'.id', $id);
        $query = $this->db->get();
        
		if ($query->num_rows() == 1) {
			$result = $query->result();
			$data = $this->mapSingleResult($result);
                        
			return $result[0];
		} else {
			return FALSE;
		}
	}
        
    function getAllIdsByShopId($shopId){
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $this->db->select('inventory.id');
        $query = $this->db->get_where(self::TABLE_NAME, array('shop_id' => $shopId, 'is_active' => 1));

        if ($query->num_rows() >0) {
            $result = $query->result();
            $ids = array();
            foreach($result as $id){
                array_push($ids, $id->id);
            }
            return $ids;
        } else {
            return FALSE;
        }
    }
	
	function findByCategoryId($category_id) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $query = $this->db->get_where(self::TABLE_NAME, array($this->column_inventory_category_id => $category_id));
	
		if ($query->num_rows() > 0) {
			$result = $query->result();
			$data = $this->mapMultipleResult($result);
	
			return $data;
		} else {
			return FALSE;
		}
	}
	
	function findByShopId($shop_id) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        $this->db->select('inventory.*');
        $this->db->select('inventory_category.category_name');
        $this->db->join('inventory_category', 'inventory_category.id = '.$this->table_name . '.inventory_category_id');
        $query = $this->db->get_where(self::TABLE_NAME, 
                            array(
                                $this->column_shop_id => $shop_id,
                                'inventory.is_active' => 1
                            )
                        );
	
		if ($query->num_rows() > 0) {
			$result = $query->result();
			$data = $this->mapMultipleResult($result);
	
			return $data;
		} else {
			return FALSE;
		}
	}
        
    function findByShopId2($shop_id) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
        }
        
        $query = $this->db->get_where(self::TABLE_NAME, array($this->column_shop_id => $shop_id, 'inventory.is_active' => 1));
                
                if ($query->num_rows() > 0) {
                    $result = $query->result();
                    return $result;
		} else {
                    return FALSE;
		}
	}
	
	function deleteById($id) {
		if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB("zeepos_client_".$session['userProfileId']);
		}
		$query = $this->db->delete(self::TABLE_NAME, array($this->column_id => $id));
		if($this->db->_error_message()){
			return false;
		}
		return $query; //return TRUE if Success, FALSE if failed
	}
	
	function getTotalResult() {
		return $this->totalResult;
	}
	
	private function getData() {
		$data = array(
            'inventory_category_id' => $this->inventory_category_id,
            'shop_id' => $this->shop_id,
            'inventory_name' => $this->inventory_name,
            'current_hr_qty' => $this->current_hr_qty,
            'hr_unit' => $this->hr_unit,
            'current_qty' => $this->current_qty,
            'unit' => $this->unit,
            'conversion' => $this->conversion,
            'minimum_stock' => $this->minimum_stock,
            'latest_restock' => $this->latest_restock,
            'inventory_value' => $this->inventory_value,
            'average_value' => $this->average_value,
            'created_date' => $this->created_date,
            'created_by' => $this->created_by,
            'updated_date' => $this->updated_date,
            'updated_by' => $this->updated_by,
            'alert_status' => $this->status_alert
		);
	
		return $data;
	}
	
	private function mapSingleResult($result) {
		$data = new InventoryModel();
		
		$data->id = $result[0]->id;
        $data->inventory_id = $result[0]->inventory_id;
        $data->inventory_category_id = $result[0]->inventory_category_id;
		$data->shop_id = $result[0]->shop_id; 
		$data->inventory_name = $result[0]->inventory_name; 
		$data->current_hr_qty = $result[0]->current_hr_qty; 
		$data->hr_unit = $result[0]->hr_unit; 
		$data->current_qty = $result[0]->current_qty; 
		$data->unit = $result[0]->unit; 
		$data->conversion = $result[0]->conversion; 
		$data->minimum_stock = $result[0]->minimum_stock; 
		$data->latest_restock = $result[0]->latest_restock; 
		$data->inventory_value = $result[0]->inventory_value; 
		$data->average_value = $result[0]->average_value; 
		$data->created_date = $result[0]->created_date; 
		$data->created_by = $result[0]->created_by;
		$data->updated_date = $result[0]->updated_date; 
		$data->updated_by = $result[0]->updated_by; 
        if (isset($result[0]->category_name)) {
            $data->category_name = $result[0]->category_name;
        }	
        if (isset($result[0]->shop_name)) {
            $data->shop_name = $result[0]->shop_name;
        }	
        
        if (isset($result[0]->alert_status)) {
            $data->status_alert = $result[0]->alert_status;
        }
	
		return $data;
	}
	
	private function mapMultipleResult2($results, $index = 1) {
		$arrayOfData = array();
	
		foreach ($results as $item) {
			$data = new InventoryModel();
			$data->resultIndex = $index; $index++;
			$data->id = $item->id;
			$data->inventory_category_id = $item->inventory_category_id;
			$data->category_name= $item->category_name;
			$data->shop_id = $item->shop_id;
			$data->inventory_name = $item->inventory_name;
			$data->current_hr_qty = $item->current_hr_qty;
			$data->hr_unit = $item->hr_unit;
			$data->current_qty = $item->current_qty;
			$data->unit = $item->unit;
			$data->conversion = $item->conversion;
			$data->minimum_stock = $item->minimum_stock;
			$data->latest_restock = $item->latest_restock;
			$data->inventory_value = $item->inventory_value;
			$data->average_value = $item->average_value;
            if (isset($item->shopName)) {
                $data->shopName = $item->shopName;
            }
            if (isset($item->category_name)) {
                $data->categoryName = $item->category_name;
            }
			
			array_push($arrayOfData, $data);
		}
	
		return $arrayOfData;
	}
	
	private function mapMultipleResult($results, $index = 1, $useCloseTime = 0, $start, $end) {
        $arrayOfData = array();
//        $defaultClientOffset = date('Z');
//        $defaultClientOffsetByHours = $defaultClientOffset / 3600;
//        if ($this->session->userdata('logged_in')){
//            $session = $this->session->userdata('logged_in');
//            if(isset($session['user_timezone_offset'])){
//                $defaultClientOffsetByHours = $session['user_timezone_offset'];
//            }
//        }
        foreach ($results as $item) {
            $item->resultIndex = $index;
            if ($useCloseTime) {
                if (isset($item->shop_id)) {
                    $this->load->model('ShopModel', '', TRUE);
                    $shop = $this->ShopModel->findById($item->shop_id);
                    if ($shop) {
                        if(!isset($item->name)){
                            $item->name = $shop->name;
                        }
                        $shopOffset = abs($shop->offset_hour);
                        $startTime = $shopOffset < 10 ? $start . ' 0' . $shopOffset . ':00:00' : $start . ' ' . $shopOffset . ':00:00';
                        $date1 = str_replace('-', '/', $end);
                        $tomorrow = date('Y-m-d', strtotime($date1 . "+1 days"));
                        $endTime = $shopOffset == 0 ? $tomorrow . ' 00:00:00' : ($shopOffset < 11 ? $tomorrow . ' 0' . ($shopOffset - 1) . ':59:00' : $tomorrow . ' ' . ($shopOffset - 1) . ':59:00');

                        if (isset($item->datetime)) {
                            $timestamp = $item->datetime;
                            if ($timestamp > $startTime && $timestamp < $endTime) {
//                                $clientDatetime = $this->DatabaseModel->getClientTimeByServerTimezone($timestamp, $defaultClientOffsetByHours);
//                                $item->datetime = $clientDatetime;
                                $index++;
                                array_push($arrayOfData, $item);
                            }
                        }
                    }
                }
            } else {
                if(!isset($item->name)){
                    $shop = $this->ShopModel->findById($item->shop_id);
                    if($shop){
                        $item->name = $shop->name;
                    }
                }
                if (isset($item->datetime)){
                    if ($item->datetime > $start.' 00:00:00' && $item->datetime < $end.' 23:59:59') {
//                        $clientDatetime = $this->DatabaseModel->getClientTimeByServerTimezone($item->datetime, $defaultClientOffsetByHours);
//                        $item->datetime = $clientDatetime;
                        $index++;
                        array_push($arrayOfData, $item);
                    }
                } else {
                    $index++;
                    array_push($arrayOfData, $item);
                }
            }
        }
        
        return $arrayOfData;
    }
        
    function getInventoryWithMinimumStockList(){
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('current_qty <=', 'minimun_stock');
        $this->db->where('is_active', '1');
        $this->db->where('alert_status', '0');
        
        $query = $this->db->get();
        
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return FALSE;
        }
    }
}