<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of admin model
 */
class DatabaseModel Extends CI_Model {

    // var $hostname = "";
    // var $username = "";
    // var $password = ""
    // var $database = "";
    // var $dbdriver = "";
    // var $dbprefix = ""
    // var $pconnect = TRUE;
    // var $db_debug = TRUE;
    // var $cache_on = TRUE;
    // var $cachedir = ""
    // var $char_set = "";
    // var $dbcollat = "";
    var $configDatabase = "";

    function __construct() {
        parent::__construct();
        $this->configDatabase['hostname'] = $this->config->item('db_hostname');
        $this->configDatabase['username'] = $this->config->item('db_deploy_user');
        $this->configDatabase['password'] = $this->config->item('db_deploy_password');
        $this->configDatabase['database'] = "kwlm_v_local";
        $this->configDatabase['dbdriver'] = "mysql";
        $this->configDatabase['dbprefix'] = "";
        $this->configDatabase['pconnect'] = TRUE;
        $this->configDatabase['db_debug'] = TRUE;
        $this->configDatabase['cache_on'] = FALSE;
        $this->configDatabase['cachedir'] = "";
        $this->configDatabase['char_set'] = "utf8";
        $this->configDatabase['dbcollat'] = "utf8_general_ci";
    }

    public function getConfigDB($database = ''){
        if($database != ''){
            $this->configDatabase['database'] = $database;
        }

        return $this->configDatabase;
    }
    
    public function getConfigDBFull($database){
        $this->configDatabase['hostname'] = 'dev.zeepos.com';
        $this->configDatabase['username'] = 'webadmin';
        $this->configDatabase['password'] = '';
        $this->configDatabase['database'] = $database;

        return $this->configDatabase;
    }
    public function getConfigDBFull2($database){
        $this->configDatabase['hostname'] = 'localhost';
        $this->configDatabase['username'] = 'root';
        $this->configDatabase['password'] = '';
        $this->configDatabase['database'] = $database;

        return $this->configDatabase;
    }

    public function getDB(){
        $configDB = $this->configDatabaseClient;
        return $this->load->database($configDB);
    }

    // public function save() {
    //     $data = $this->getSaveData();
    //     $this->db->insert(self::TABLE_NAME, $data);

    //     return $this->id;
    // }

    // public function bulkSave($arrayOf) {
    //     $arrayOfData = array();
    //     foreach ($arrayOf as $item) {
    //         $data = $item->getSaveData();
    //         array_push($arrayOfData, $data);
    //     }

    //     return $this->db->insert_batch(self::TABLE_NAME, $arrayOfData);
    // }

    // public function update($id) {
    //     $data = array(
    //         'username' => $this->username,
    //     );
    //     $this->db->where('id', $id);

    //     return $this->db->update(self::TABLE_NAME, $data);
    // }

    // public function changePassword($id){
    //     $data = array(
    //         'password' => $this->password
    //     );
    //     $this->db->where('id', $id);

    //     return $this->db->update(self::TABLE_NAME, $data);
    // }

    // public function findById($id) {
    //     $this->db->select('*');
    //     $this->db->from(self::TABLE_NAME);
    //     $this->db->where('id', $id);
    //     $query = $this->db->get();

    //     if ($query->num_rows() == 1) {
    //         $result = $query->result();
    //         $data = $this->mapSingleResult($result);

    //         return $data;
    //     } else {
    //         return FALSE;
    //     }
    // }

    // public function findByUsername($username) {
    //     $this->db->select('*');
    //     $this->db->from(self::TABLE_NAME);
    //     $this->db->where('username', $username);
    //     $query = $this->db->get();

    //     if ($query->num_rows() == 1) {
    //         $result = $query->result();
    //         $data = $this->mapSingleResult($result);

    //         return $data;
    //     } else {
    //         return FALSE;
    //     }
    // }

    // public function deleteById($id) {
    //     $query = $this->db->delete(self::TABLE_NAME, array('id' => $id));
    //     if($this->db->_error_message()){
    //         return false;
    //     }
    //     return $query; //return TRUE if Success, FALSE if failed
    // }

    // public function getTotalResult() {
    //     return $this->totalResult;
    // }

    // private function getData() {
    //     $data = array(
    //         'id' => $this->id,
    //         'username' => $this->username,
    //         'password' => $this->password
    //     );

    //     return $data;
    // }

    // private function getSaveData() {
    //     $data = array(
    //         'username' => $this->username,
    //         'password' => $this->password
    //     );

    //     return $data;
    // }

    // private function mapSingleResult($result) {
    //     $data = new AdminModel();
    //     $data->id = $result[0]->id;
    //     $data->username = $result[0]->username;
    //     $data->password = $result[0]->password;

    //     return $data;
    // }

    // private function mapMultipleResult($results, $index = 1) {
    //     $arrayOfData = array();

    //     foreach ($results as $item) {
    //         $data = new AdminModel();
    //         $data->resultIndex = $index; $index++;
    //         $data->id = $item->id;
    //         $data->username = $item->username;
    //         $data->password = $item->password;
        
    //         array_push($arrayOfData, $data);
    //     }

    //     return $arrayOfData;
    // }

    function getClientTimeByServerTimezone($serverDatetime, $clientOffsetInHours){
        $serverOffset = 0;
        $serverOffsetByHours = $serverOffset / 3600;
        $clientDatetime = strtotime($serverDatetime) - ($serverOffsetByHours - $clientOffsetInHours) * 3600;
        return date('Y-m-d H:i:s', $clientDatetime);
    }
    
    function getServerTimeByClientTimezone($clientDatetime, $clientOffsetInHours){
        $serverOffset = date('Z');
        $serverOffsetByHours = $serverOffset / 3600;
        $serverDatetime = strtotime($clientDatetime) + ($serverOffsetByHours - $clientOffsetInHours) * 3600;
        return date('Y-m-d H:i:s', $serverDatetime);
    }
}

?>
