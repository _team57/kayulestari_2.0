<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @dzafiars
 */
class AnggotaModel Extends CI_Model {

    const TABLE_NAME = 'anggota';

    var $totalResult = 0;
    var $resultIndex = 1; //start from 1
    
    var $no_anggota = '';
    var $koperasi = '';
    var $nama = '';
    var $tanggal_masuk = '';
    var $created_time = '';
	var $update_time = '';
	var $profil_vstatus = '';
	var $profile = null;
	var $user_id = '';
	var $foto = '';
	var $foto_ktp = '';
	var $foto_pernyataan = '';
	var $profile_note = '';

    function __construct() {
        parent::__construct();
        $this->load->database();
        
//        $tempArray = array(
//            'kelurahan' => 0.0,
//            'kecamatan' => 0,
//            'kabupaten/kota' => false,
//            'provinsi' => 0,
//            'no_ktp' => 0,
//            'alamat' => 0,
//            'telepon' => 0,
//            'hp' => 0,
//            'pekerjaan' => 0,
//        );
//
//        $this->extra = (object) $tempArray;
        $this->load->model('DatabaseModel', '', TRUE);
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->load->database($configDB, TRUE);
        }
    }

    public function setDB($database = ''){
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->db = $this->load->database($configDB, TRUE);
        }
    }
 
    public function findAll() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->from(self::TABLE_NAME);
        $query = $this->db->get();
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
 
    public function findAllWithPaging($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select("SQL_CALC_FOUND_ROWS 
            no_anggota, 
            koperasi, 
            nama, 
            tanggal_masuk, 
            profile,
            IFNULL(foto, 'Tidak Ada') as foto,
            IFNULL(foto_ktp, 'Tidak Ada') as foto_ktp,
            IFNULL(foto_pernyataan, 'Tidak Ada') as foto_pernyataan", false
        );
        
        if ($sVal != '') {
            $this->db->where('('.
                'no_anggota LIKE "%'.$sVal.'%" OR '.
                'koperasi LIKE "%'.$sVal.'%" OR '.
                'nama LIKE "%'.$sVal.'%" OR '.
                'tanggal_masuk LIKE "%'.$sVal.'%"'.
            ')');
        }
        
        $this->db->order_by($order, $dir);
        $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
                
        $result = $query->result();
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        return $this->mapMultipleResult($result, $offset + 1);
    }
    
    public function save() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = $this->getSaveData();
        $this->db->insert(self::TABLE_NAME, $data);

        return $this->db->insert_id();
    }

    public function update() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = array(
            'username' => $this->username,
            'password' => $this->password,
            'name' => $this->name,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );
        $this->db->where('id', $this->id);

        return $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function findByNoAnggota($no_anggota) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('no_anggota', $no_anggota);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            $data = $this->mapSingleResult($result);

            return $data;
        } else {
            return FALSE;
        }
    }

    public function deleteByNoAnggota($no_anggota) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $query = $this->db->delete(self::TABLE_NAME, array('no_anggota' => $no_anggota));
        if($this->db->_error_message()){
            return false;
        }
        return $query; //return TRUE if Success, FALSE if failed
    }

    public function getTotalResult() {
        return $this->totalResult;
    }

    private function getData() {
        $data = array(
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            // 'profile_id' => $this->profile_id,
            'role_id' => $this->role_id,
            'profile' => json_encode($this->profile),
            'password' => $this->password
        );

        return $data;
    }

    private function getSaveData() {
        $data = array(
            'username' => $this->username,
            'name' => $this->name,
            'password' => $this->password,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );

        return $data;
    }

    private function mapSingleResult($result) {
        $data = new AnggotaModel();
        $data = $result[0];
        if($result[0]->profile){
            $data->profile = json_decode($result[0]->profile);
        }
        return $data;
    }

    private function mapMultipleResult($results, $index) {
        $arrayOfData = array();

        foreach ($results as $item) {
            $data = new AnggotaModel();
            $data = $item;
            if($item->profile){
                $data->profile = json_decode($item->profile);
            }
            $data->resultIndex = $index;$index++;
            array_push($arrayOfData, $data);
        }

        return $arrayOfData;
    }
    
}

?>
