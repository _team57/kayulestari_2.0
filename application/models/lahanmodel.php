<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @dzafiars
 */
class LahanModel Extends CI_Model {

    const TABLE_NAME = 'lahan';

    var $totalResult = 0;
    var $resultIndex = 1; //start from 1
    
    var $id = '';
    var $no_anggota = '';
    var $nama_lahan = '';
    var $foto_item = '';
    var $foto_sertifikat = '';
    var $foto_sketsa = '';
    var $no_sertifikat = '';
    var $kelurahan = '';
    var $kecamatan = '';
    var $kab = '';
    var $provinsi = '';
    var $luas_lahan = '';
    var $jenis_sertifikat = '';
    var $data_vstatus = '';
    var $pohon_vstatus = '';
    var $lat = '';
    var $lng = '';
    var $alt = '';
    var $data = '';
    var $update_time = '';
    var $user_id = '';
    var $data_note = '';
    var $pohon_note = '';

    var $nama_anggota = '';
    
    function __construct() {
        parent::__construct();
        $this->load->database();
        
        $this->load->model('DatabaseModel', '', TRUE);
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->load->database($configDB, TRUE);
        }
    }

    public function setDB($database = ''){
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->db = $this->load->database($configDB, TRUE);
        }
    }
 
    public function findAll() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        
        $this->db->from(self::TABLE_NAME);
        $query = $this->db->get();
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
 
    public function findAllWithPaging($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select("SQL_CALC_FOUND_ROWS 
            anggota.nama as nama_anggota,
            id, 
            lahan.no_anggota, 
            nama_lahan, 
            IFNULL(foto_sertifikat, 'Tidak Ada') as foto_sertifikat,
            IFNULL(foto_sketsa, 'Tidak Ada') as foto_sketsa,
            no_sertifikat,
            kelurahan,
            kecamatan,
            kab,
            provinsi,
            luas_lahan,
            jenis_sertifikat,
            lat,
            lng,
            alt,
            data", false
        );
        $this->db->join("anggota","anggota.no_anggota = lahan.no_anggota");
        
        if ($sVal != '') {
            $this->db->where('('.
                'anggota.nama LIKE "%'.$sVal.'%" OR '.
                'nama_lahan LIKE "%'.$sVal.'%" OR '.
                'lahan.no_anggota LIKE "%'.$sVal.'%"'.
            ')');
        }
        
        $this->db->order_by($order, $dir);
        $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
        
        $result = $query->result();

        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        return $this->mapMultipleResult($result, $offset + 1);
    }
    
    public function findLahanForReport($offset, $limit, $sVal, $order, $dir, $pendata) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        
        $this->db->select("SQL_CALC_FOUND_ROWS
            lahan.id as lahan_id,
            lahan.no_anggota, 
            anggota.nama AS nama_anggota, 
            lahan.nama_lahan, 
            case WHEN foto_sertifikat='' 
                then 'Tidak Ada' 
                else 'Ada' 
                end foto_sertifikat,
            case WHEN foto_sketsa='' 
                then 'Tidak Ada' 
                else 'Ada' 
                end foto_sketsa,
            no_sertifikat, 
            kelurahan, 
            kecamatan, 
            kab,
            provinsi, 
            luas_lahan, 
            jenis_sertifikat, 
            lat, 
            lng, 
            alt, 
            data,
            user.name pendata", false
        );
        $this->db->join("anggota", "anggota.no_anggota = lahan.no_anggota");
        $this->db->join("user", "user.id = lahan.user_id");
        $this->db->where("lahan.user_id IS NOT NULL");
        
        if($pendata != 0){
            $this->db->where("lahan.user_id", $pendata); 
        }
        
        if ($sVal != '') {
            $this->db->where('('.
                'anggota.nama LIKE "%'.$sVal.'%" OR '.
                'nama_lahan LIKE "%'.$sVal.'%" OR '.
                'lahan.no_anggota LIKE "%'.$sVal.'%"'.
            ')');
        }
        
        $this->db->order_by($order, $dir);
        if($limit > 0){
            $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
        } else {
            $query = $this->db->get(self::TABLE_NAME);
        }
        
        $result = $query->result();
//        echo $this->db->last_query();exit;
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        return $this->mapMultipleResult($result, $offset + 1);
    }
    
    public function findFotoLahan($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select(""
            . "anggota.no_anggota, "
            . "anggota.nama, "
            . "foto_item, "
            . "id, "
            . "nama_lahan, "
            . "foto_sertifikat, "
            . "foto_sketsa"
        );
        $this->db->join("anggota","anggota.no_anggota = lahan.no_anggota");

        if ($sVal != '') {
            $this->db->where('('.
                'anggota.no_anggota LIKE "%'.$sVal.'%" OR '.
                'anggota.nama LIKE "%'.$sVal.'%" OR '.
                'nama_lahan LIKE "%'.$sVal.'%"'.
            ')');
        }
        
        $this->db->order_by($order, $dir);
        if ($limit > 0){
            $query = $this->db->get(self::TABLE_NAME, $limit, $offset);
        } else {    
            $query = $this->db->get(self::TABLE_NAME);
        }
        $result = $query->result();
        
        $this->db->select("count(DISTINCT lahan.id) AS total_result");
        
        $queryTotal = $this->db->get(self::TABLE_NAME);
        $totalResult = $queryTotal->result();
        $this->totalResult = $totalResult[0]->total_result;
        
        return $this->mapMultipleResultForFoto($result, $offset+1);
    }
 
    public function getVerificationReport($startDate, $endDate, $offset, $limit, $sVal, $order, $dir, $type) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        
        if($startDate != '0000-00-00' AND $endDate != '0000-00-00'){
            $timestampRule = " AND pohon.timestamp BETWEEN '".$startDate." 00:00:00' AND '".$endDate." 23:59:59'";
        } else {
            $timestampRule = " ";
        }
        if ($limit > 0){
            $limitRule = " LIMIT ".$limit." OFFSET ".$offset;
        } else {
            $limitRule = "";
        }
        
        $query = "
            SELECT
                a.pendata,
                a.no_anggota,
                a.nama,
                a.nama_lahan,
                a.assigned,
                IFNULL(d.verified_valid, 0) AS verified_valid,
                IFNULL(b.verified_invalid, 0) AS verified_invalid,
                IFNULL(c.unverified, 0) AS unverified
            FROM
                (
                    SELECT
                        `user`.`name` AS pendata,
                        lahan.id AS lahan_id,
                        lahan.no_anggota AS no_anggota,
                        anggota.nama,
                        lahan.nama_lahan,
                        COUNT(pohon.id) AS assigned
                    FROM
                        pohon
                    JOIN lahan ON lahan.id = pohon.lahan_id
                    JOIN anggota ON anggota.no_anggota = lahan.no_anggota
                    JOIN `user` ON `user`.id = pohon.user_id
                    WHERE
                        is_valid = 1".$timestampRule."
                    GROUP BY
                        lahan.id
                ) AS a
            LEFT JOIN (
                SELECT
                    pohon.lahan_id,
                    COUNT(pohon.id) AS verified_invalid
                FROM
                    pohon
                JOIN lahan ON lahan.id = pohon.lahan_id
                WHERE
                    is_valid = 1
                AND verificator IS NOT NULL
                AND keliling = 0".$timestampRule."
                GROUP BY
                    lahan.id
            ) AS b ON b.lahan_id = a.lahan_id
            LEFT JOIN (
                SELECT
                    pohon.lahan_id,
                    COUNT(pohon.id) AS unverified
                FROM
                    pohon
                JOIN lahan ON lahan.id = pohon.lahan_id
                WHERE
                    is_valid = 1
                AND verificator IS NULL".$timestampRule."
                GROUP BY
                    lahan.id
            ) AS c ON c.lahan_id = a.lahan_id
            LEFT JOIN (
                SELECT
                    pohon.lahan_id,
                    COUNT(pohon.id) AS verified_valid
                FROM
                    pohon
                JOIN lahan ON lahan.id = pohon.lahan_id
                WHERE
                    is_valid = 1
                AND verificator IS NOT NULL
                AND keliling != 0".$timestampRule."
                GROUP BY
                    lahan.id
            ) AS d ON d.lahan_id = a.lahan_id
            GROUP BY
                a.nama,
                a.lahan_id
            ";
        
        $result = $this->db->query($query)->result();
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;

        $data = $this->mapMultipleResultForReport($result, $offset, $limit);
        return $data;
    }
    
    public function save() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = $this->getSaveData();
        $this->db->insert(self::TABLE_NAME, $data);

        return $this->db->insert_id();
    }

    public function update() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = array(
            'username' => $this->username,
            'password' => $this->password,
            'name' => $this->name,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );
        $this->db->where('id', $this->id);

        return $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function findById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            $data = $this->mapSingleResult($result);

            return $data;
        } else {
            return FALSE;
        }
    }

    public function deleteById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $query = $this->db->delete(self::TABLE_NAME, array('id' => $id));
        if($this->db->_error_message()){
            return false;
        }
        return $query; //return TRUE if Success, FALSE if failed
    }

    public function getTotalResult() {
        return $this->totalResult;
    }

    private function getData() {
        $data = array(
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            // 'profile_id' => $this->profile_id,
            'role_id' => $this->role_id,
            'profile' => json_encode($this->profile),
            'password' => $this->password
        );

        return $data;
    }

    private function getSaveData() {
        $data = array(
            'username' => $this->username,
            'name' => $this->name,
            'password' => $this->password,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );

        return $data;
    }

    private function mapSingleResult($result) {
        $data = new AnggotaModel();
        $data = $result[0];
        if($result[0]->profile){
            $data->profile = json_decode($result[0]->profile);
        }
        return $data;
    }

    private function mapMultipleResult($result, $index) {
        $arrayOfData = array();
        $resultIndex = $index;
        foreach ($result as $item){
            $data = new LahanModel();
            $data = $item;
            $data->resultIndex = $resultIndex; $resultIndex++;
            
            array_push($arrayOfData, $data);
        }
        return $arrayOfData;
    }

    private function mapMultipleResultForReport($result, $offset, $limit) {
        $arrayOfData = array();
        $resultIndex = 1;
        foreach ($result as $item){
            $data = new LahanModel();
            $data = $item;
            $data->resultIndex = $resultIndex; $resultIndex++;
            
            array_push($arrayOfData, $data);
        }
        if($limit > 0){
            return array_slice($arrayOfData, $offset, $limit);
        } else {
            return $arrayOfData;
        }
    }

    private function mapMultipleResultForFoto($results, $index) {
        $arrayOfData = array();
        $resultIndex = $index;
        foreach ($results as $item) {
            $data = new LahanModel();
            $jumlah_foto_sertifikat = 0;
            $jumlah_foto_sketsa = 0;
            $jumlah_lain = 0;
            
            $data = $item;
            $data->nama_anggota = $item->nama;
            if($item->foto_item){
                $data_foto = json_decode($item->foto_item);
                foreach ($data_foto as $foto) {
                    if($foto->name == "Foto Sertifikat"){
                        $jumlah_foto_sertifikat++;
                    } else if($foto->name == "Foto Sketsa"){
                        $jumlah_foto_sketsa++;
                    } else {
                        $jumlah_lain++;
                    }
                }
            }
            if($item->foto_sertifikat){
                $jumlah_foto_sertifikat++;
            }
            if($item->foto_sketsa){
                $jumlah_foto_sketsa++;
            }
            $data->foto_sertifikat = $jumlah_foto_sertifikat > 0 ? "Ada" : "Tidak Ada";
            $data->jumlah_foto_sertifikat = $jumlah_foto_sertifikat;
            $data->foto_sketsa = $jumlah_foto_sketsa > 0 ? "Ada" : "Tidak Ada";
            $data->jumlah_foto_sketsa = $jumlah_foto_sketsa;
            $data->resultIndex = $resultIndex; $resultIndex++;
            array_push($arrayOfData, $data);
        }
        return $arrayOfData;
    }
    
    
}
?>
