<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Description of UserModel
 * This is model user using object oriented paradigm
 * @author Alfath Innovative
 * @author Muhamad Fajar
 */
class UserModel Extends CI_Model {

    const TABLE_NAME = 'user';

    var $totalResult = 0;
    var $resultIndex = 1; //start from 1
    
    var $id = '';
    var $username = '';
    var $password = '';
    var $name = '';
    var $role_id = '';
	var $spv_id = '';

    function __construct($database = '') {
        parent::__construct();
        $this->load->database();
        
        $this->load->model('DatabaseModel', '', TRUE);
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->load->database($configDB, TRUE);
        }
    }

    public function setDB($database = ''){
        if($database != ''){
            $configDB = $this->DatabaseModel->getConfigDBFull($database);
            $this->db = $this->load->database($configDB, TRUE);
        }
    }
 
    public function findAll() {
        $this->db->from(self::TABLE_NAME);
        $query = $this->db->get();
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
 
    public function findAllWithPaging($offset, $limit, $sVal, $order, $dir) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select("SQL_CALC_FOUND_ROWS u1.*,
            u2.name as spv_name,
            CASE u1.role_id
            WHEN '1' THEN
                'Supervisor/Verificator'
            WHEN '0' THEN
                'Pendata'
            WHEN '2' THEN
                'Admin'
            END AS role", false
        );
        $this->db->join('user u2', 'u2.id = u1.spv_id', 'left');
        
        if ($sVal != '') {
            $this->db->where('('.
                'u1.username LIKE "%'.$sVal.'%" OR '.
                'u1.name LIKE "%'.$sVal.'%" OR '.
                'u1.role_id LIKE "%'.$sVal.'%" OR '.
                'u1.spv_id LIKE "%'.$sVal.'%" OR '.
                'u2.name LIKE "%'.$sVal.'%"'.
            ')');
        }
        
        $this->db->order_by($order, $dir);
        $query = $this->db->get(self::TABLE_NAME." u1", $limit, $offset);
                
        $result = $query->result();
        
        $queryTotalResult = $this->db->query('SELECT FOUND_ROWS() AS `count`');
        $this->totalResult = $queryTotalResult->row()->count;
        
        return $this->mapMultipleResult($result, $offset + 1);
    }
    
    public function findAllSpv() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('id, name as spv_name');
        $this->db->where('role_id', '1');
        $query = $this->db->get(self::TABLE_NAME);
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
    
    public function findAllPendata() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('id, name as pendata_name');
        $this->db->where('role_id', '0');
        $query = $this->db->get(self::TABLE_NAME);
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
    
    public function findAllVerif() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('id, name');
        $this->db->where("username LIKE 'verif%'");
        $query = $this->db->get(self::TABLE_NAME);
        $result = $query->result();
        
        return $this->mapMultipleResult($result);
    }
    
    public function save() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = $this->getSaveData();
        $this->db->insert(self::TABLE_NAME, $data);

        return $this->db->insert_id();
    }

    public function update() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $data = array(
            'username' => $this->username,
            'password' => $this->password,
            'name' => $this->name,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );
        $this->db->where('id', $this->id);

        return $this->db->update(self::TABLE_NAME, $data);
    }
    
    public function findById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('id', $id);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            $data = $this->mapSingleResult($result);

            return $data;
        } else {
            return FALSE;
        }
        // return 1;
    }

    public function findByUsername($username) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $this->db->select('*');
        $this->db->from(self::TABLE_NAME);
        $this->db->where('username', $username);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            $result = $query->result();
            $data = $this->mapSingleResult($result);

            return $data;
        } else {
            return FALSE;
        }
    }

    public function deleteById($id) {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $query = $this->db->delete(self::TABLE_NAME, array('id' => $id));
        if($this->db->_error_message()){
            return false;
        }
        return $query; //return TRUE if Success, FALSE if failed
    }

    public function getTotalResult() {
        return $this->totalResult;
    }

    private function getData() {
        $data = array(
            'id' => $this->id,
            'username' => $this->username,
            'name' => $this->name,
            // 'profile_id' => $this->profile_id,
            'role_id' => $this->role_id,
            'profile' => json_encode($this->profile),
            'password' => $this->password
        );

        return $data;
    }

    private function getSaveData() {
        $data = array(
            'username' => $this->username,
            'name' => $this->name,
            'password' => $this->password,
            'role_id' => $this->role_id,
            'spv_id' => $this->spv_id
        );

        return $data;
    }

    private function mapSingleResult($result) {
        $data = new UserModel();
        $data = $result[0];
        return $data;
    }

    private function mapMultipleResult($results, $index) {
        $arrayOfData = array();

        foreach ($results as $item) {
            $data = new UserModel();
            $data = $item;
            $data->resultIndex = $index;$index++;
            array_push($arrayOfData, $data);
        }

        return $arrayOfData;
    }

    
    /**
     * function login
     * to check the login credential
     */
    function login() {
        if($this->session->userdata('logged_in')){
            $session = $this->session->userdata('logged_in');
            $this->setDB($session['select_db']);
        }
        $query = $this->db->get_where(self::TABLE_NAME, array(
            'username' => $this->username,
            'password' => $this->password
            ));

        if ($query->num_rows() == 1) {
            $result = $query->result();

            return $result[0];
        } else {

            return FALSE;
        }
    }
}

?>
