<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
        <meta charset="utf-8" />
        <title><?= isset($pageTitle) ? $pageTitle : "SOBI" ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
        <link rel="apple-touch-icon" href="<?= base_url() ?>/resources/template/pages/ico/60.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>/resources/template/pages/ico/76.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>/resources/template/pages/ico/120.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>/resources/template/pages/ico/152.png">
        <link rel="icon" type="image/x-icon" href="favicon.ico" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-touch-fullscreen" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="default">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?= base_url() ?>/resources/template/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
        <link href="<?= base_url() ?>/resources/template/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
        <link class="main-stylesheet" href="<?= base_url() ?>/resources/template/pages/css/pages.css" rel="stylesheet" type="text/css" />
        <!--[if lte IE 9]>
            <link href="<?= base_url() ?>/resources/template/pages/css/ie9.css" rel="stylesheet" type="text/css" />
        <![endif]-->
        <script type="text/javascript">
            window.onload = function()
            {
                // fix for windows 8
                if (navigator.appVersion.indexOf("Windows NT 6.2") != -1)
                    document.head.innerHTML += '<link rel="stylesheet" type="text/css" href="<?= base_url() ?>/resources/template/pages/css/windows.chrome.fix.css" />'
            }
        </script>
    </head>
    <body class="fixed-header ">
        <div class="login-wrapper ">
            <!-- START Login Background Pic Wrapper-->
            <div class="bg-pic">
                <!-- START Background Pic-->
                <img src="<?= base_url() ?>/resources/template/assets/img/demo/login-wallpaper.jpg" alt="" class="lazy">
                <!-- END Background Pic-->
                <!-- START Background Caption-->
                <div class="bg-caption pull-bottom sm-pull-bottom text-white p-l-20 m-b-20">
                    <h2 class="semi-bold text-white">
                        SOBI</h2>
                    <p class="small">
                        images Displayed are solely for representation purposes only, All work copyright of respective owner, otherwise © 2016 Cartenz.
                    </p>
                </div>
                <!-- END Background Caption-->
            </div>
            <!-- END Login Background Pic Wrapper-->
            <!-- START Login Right Container-->
            <div class="login-container bg-white">
                <div class="p-l-50 m-l-20 p-r-50 m-r-20 p-t-50 m-t-30 sm-p-l-15 sm-p-r-15 sm-p-t-40">
                    <img src="<?= base_url() ?>/resources/template/assets/img/logo.png" alt="logo" data-src="<?= base_url() ?>/resources/template/assets/img/logo.png" data-src-retina="<?= base_url() ?>/resources/template/assets/img/logo_2x.png" width="78" height="22">
                    <p class="p-t-35">Sign into your SOBI account</p>
                    <!-- START Login Form -->
                    <form method="POST" id="form-login" class="p-t-15" role="form" action="<?= base_url(); ?>index.php/auth/postLogin">
                        <!-- START Form Control-->
                        <?php
                        $errormsg = validation_errors();
                        if ($errormsg != '') {
                            echo validation_errors();
                        };
                        ?>
                        <?php echo isset($loginMessage) ? $loginMessage : ''; ?>
                        <div class="form-group form-group-default">
                            <label>Username</label>
                            <div class="controls">
                                <input type="text" name="username" placeholder="Username" class="form-control" required>
                            </div>
                        </div>
                        <div class="form-group form-group-default">
                            <label>Password</label>
                            <div class="controls">
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                        </div>
                        <div class="form-group form-group-default">
                            <label>Pilih DB</label>
                            <div class="controls">
                                <select name="select_db" class="form-control">
                                    <option value="kwlm">KWLM</option>
                                    <option value="khjl_merged">KHJL</option>
<!--                                    <option value="kayulestari_kwlm_prod">KWLM 2017</option>
                                    <option value="kwlm_v">KWLM sebelum 2017</option>
                                    <option value="kayulestari2">KHJL All</option>
                                    <option value="kayu2_v">KHJL Proses</option>-->
                                </select>
                            </div>
                        </div>
                        <!-- END Form Control-->
                        <input type="hidden" name="redirectURL" value="<?php if ($this->session->flashdata('redirectURL')) {
                            echo $this->session->flashdata('redirectURL');
                        } ?>">
                        <button class="btn btn-primary btn-cons m-t-10" type="submit">Sign in</button>
                        <!--<a class="btn btn-secondary btn-cons m-t-10" href="./passwordForgot">Forgot Password</a>-->
                    </form>
                    <!--END Login Form-->
                </div>
            </div>
            <!-- END Login Right Container-->
        </div>
        <!-- BEGIN VENDOR JS -->
        <script src="<?= base_url() ?>/resources/template/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>/resources/template/assets/plugins/bootstrap-select2/select2.min.js"></script>
        <script type="text/javascript" src="<?= base_url() ?>/resources/template/assets/plugins/classie/classie.js"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>/resources/template/assets/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <!-- END VENDOR JS -->
        <script src="<?= base_url() ?>/resources/template/pages/js/pages.min.js"></script>
    </body>
</html>