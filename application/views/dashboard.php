<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <input type="hidden" id="site-url" value="<?= site_url(); ?>">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>SOBI - <?php echo $title; ?></p>
                        </li>
                    <li>
                        <a href="<?= site_url(''); ?>" class="active">Dashboard</a>
                    </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
      <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg bg-white">
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">
                        Dashboard
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <form id="form-date">
                    <div class="row row-offcanvas">
                        <ul class="nav nav-pills nav-custom" role="tablist" id="date-filter">
                            <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">Hari Ini</a></li>
                            <li role="presentation"><a href="#yesterday" aria-controls="yesterday" role="tab" data-toggle="tab">Kemarin</a></li>
                            <li role="presentation"><a href="#this-week" aria-controls="this-week" role="tab" data-toggle="tab">Pekan Ini</a></li>
                            <li role="presentation"><a href="#previous-week" aria-controls="previous-week" role="tab" data-toggle="tab">Pekan Lalu</a></li>
                            <li role="presentation"><a href="#this-month" aria-controls="this-month" role="tab" data-toggle="tab">Bulan Ini</a></li>
                            <li role="presentation"><a href="#previous-month" aria-controls="previous-month" role="tab" data-toggle="tab">Bulan Lalu</a></li>
                            <li role="presentation"><a href="#date-range" aria-controls="date-range" role="tab" data-toggle="tab">Rentang Waktu</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="today"></div>
                            <div role="tabpanel" class="tab-pane" id="yesterday"></div>
                            <div role="tabpanel" class="tab-pane" id="this-week"></div>
                            <div role="tabpanel" class="tab-pane" id="previous-week"></div>
                            <div role="tabpanel" class="tab-pane" id="this-month"></div>
                            <div role="tabpanel" class="tab-pane" id="previous-month"></div>
                            <div role="tabpanel" class="tab-pane" id="date-range">
                                <div class="col-md-5" style="padding-left:15px;">
                                    <div class="input-prepend input-group">
                                        <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                        <input type="text" style="width: 100%" name="daterangepicker" id="daterangepicker" class="form-control" value="<?php if($daterange){$daterange = explode(' - ', $daterange); echo date('m/d/Y', strtotime($daterange[0])).' - '.date('m/d/Y', strtotime($daterange[1]));} else {echo date('m/d/Y', strtotime(date('m/d/Y'))-86400*0 ).' - '.date('m/d/Y');} ?>" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-7">
                            <button class="btn btn-primary btn-cons" id="send-report">Send Report</button>
                        </div> -->
                        <br>
<!--                        <div id="filter-shop-tab" class="col-md-6" style="padding-left:15px;">
                            <label>Pilih Toko</label>
                            <select id="shop-filter" class="selectpicker">
                                <option value="0">Semua Toko</option>
                                <?php 
                                    foreach ($shops as $shop) {
                                        echo '<option value="'.$shop->id.'">'.$shop->name.'</option>';
                                    }
                                ?>
                            </select>
                        </div>-->
                    </div><br>
                    </form>
                    <div id="dashboard-analisis" style="display:none;">
                        <h3>Ringkasan Pendataan</h3>
                        <hr>
                        <div class="row row-offcanvas">
                            <div class="col-md-12">
                                <div id="gross-sales-tab" class="col-md-4">
                                    <div class=" panel panel-default">
                                        <div class="padding-25">
                                            <h4 class="text-center no-margin" id="pohon-terdata"></h4>
                                            <div class="no-margin padding-5" style="padding-left:25px; padding-right:25px;">
                                                <hr class="no-margin ">
                                            </div>
                                            <h5 class="text-center no-margin text-black" style="cursor: pointer" id="dashboard-total-pohon">Total Pohon Terdata</h5>
                                            <span id="tooltip-pohon" class="tooltiptext" style="display: none; width: 85%;background-color: #f0eded; color: #121010;text-align: center;padding: 5px 0;border-radius: 6px;position: absolute;z-index: 1;">Merupakan total pohon terdata pada filter waktu yang terpilih</span>
                                        </div>
                                    </div>
                                </div>
                                <div id="discount-tab" class="col-md-4">
                                    <div class=" panel panel-default">
                                        <div class="padding-25">
                                            <h4 class="text-center no-margin" id="lahan-terdata"></h4>
                                            <div class="no-margin padding-5" style="padding-left:25px; padding-right:25px;">
                                                <hr class="no-margin ">
                                            </div>
                                            <h5 class="text-center no-margin text-black" style="cursor: pointer" id="dashboard-total-lahan">Total Lahan Terdata</h5>
                                            <span id="tooltip-lahan" class="tooltiptext" style="display: none; width: 85%;background-color: #f0eded; color: #121010;text-align: center;padding: 5px 0;border-radius: 6px;position: absolute;z-index: 1;">Merupakan total lahan terdata pada filter waktu yang terpilih</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h3>Ringkasan Verifikasi</h3>
                        <hr>
                        <div id="top-item-tab" class="col-md-12">
                            <div class="widget-11-2 panel panel-default">

                                <div class="padding-25">
                                    <div class="pull-left">
                                        <h5 class="no-margin panel-title">Top Item</h5>
                                        <br>
<!--                                        <p id="total-penjualan-kotor-menu">Total Penjualan Kotor Menu : Rp 0,00</p>
                                        <p id="total-penjualan-kotor-sub-menu">Total Penjualan Kotor Sub Menu : Rp 0,00</p>
                                        <p id="total-penjualan-kotor-all-menu">Total : Rp 0,00</p>-->
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="auto-overflow widget-11-2-table">
                                    <table class="table table-condensed table-hover">
                                        <thead>
                                            <tr>
<!--                                                <th class="font-montserrat all-caps col-md-2">No</th>
                                                <th class="font-montserrat all-caps col-md-3">Kategori</th>
                                                <th class="font-montserrat all-caps col-md-3">Item</th>
                                                <th class="font-montserrat all-caps col-md-2">Sales</th>
                                                <th class="font-montserrat all-caps col-md-2">Volume</th>-->
                                            </tr>
                                        </thead>
                                        <tbody id="top-item">

                                        </tbody>
                                    </table>
                                </div>
                            </div>                                
                        </div>                            
                    </div>
                    <div id="no-dashboard-analisis" style="display:none;">
                        <h3>Tidak ada data pada range tanggal tersebut.</h3>
                        <h4>Silahkan pilih tanggal lain.</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade fill-in" id="modalLoading" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body text-center">
                <img src="<?= base_url(); ?>resources/progress/progress-circle-success.svg" alt="loading">
                <h5>Loading.....</h5>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>