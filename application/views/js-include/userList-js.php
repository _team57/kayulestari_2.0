<script type="text/javascript">
    var table;
    function userDataTable () {
	    table = $('#user_table').addClass('nowrap').DataTable({
                "responsive": {
                    details: {
                        type: 'inline',
                        renderer: function (api, rowIdx) {
                            var theRow = api.row(rowIdx);

                            var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                                var header = $(api.column(cell.column).header());

                                return '<tr>' +
                                    '<td><b>' +
                                    header.text() + ':' +
                                    '</b></td> ' +
                                    '<td>' +
                                    $( api.cell( cell ).node() ).html() +
                                    '</td>' +
                                    '</tr>';
                            }).toArray().join('');

                            return data ?
                                $('<table/>').append(data) :
                                false;
                        }
                    }
                },
                "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/user/ajaxUserDataTable/",
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter:</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['username']);
	    		$('td:eq(2)', nRow).html(aData['name']);
	    		$('td:eq(3)', nRow).html(aData['role_id']);
	    		$('td:eq(4)', nRow).html(aData['role']);
	    		$('td:eq(5)', nRow).html(aData['spv_id']);
                $('td:eq(6)', nRow).html(aData['spv_name']);
	    		$('td:eq(7)', nRow).html("<button data-id="+aData['id']+" class='btn btn-complete btn-sm btn-cons triggerEdit' data-toggle='modal' data-target='#editUser'>Edit <i class='fs-14 fa fa-magic'></span></button>");
                $('td:eq(8)', nRow).html("<button data-id="+aData['id']+" class='btn btn-danger btn-sm btn-cons triggerDelete' data-toggle='modal' data-target='#deleteUserModal'>Delete <i class='fs-14 pg-trash'></span></button>");
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0,7,8] }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){
        $('.selectpicker').select2();
        userDataTable();
        
        $.validate({
            form : '#form, #form-edit, #form-add',
            modules : 'security',
            onError :   function($form) {
                return false;
            },
            rules : {
                'shop[]': {
                    required: true,
                    minlength: 1
                }
            },
            onSuccess :   function($form) {
                if( $form.attr('id').localeCompare('form-edit') == 0 ){
                    submitUpdate();  
                }else if($form.attr('id').localeCompare('form-add') == 0){
                    submitCreate();
                }

                return false;
            }
        });
    
        $(document).on('click','.triggerAdd', function () { 
            $('#addUser').modal('show');
        });
                
        function submitCreate(){
            var formData = new FormData($("form#form-add")[0]);
            $('#addMenu').modal('hide');
            $("form#form-add")[0].reset();

            $.ajax({
                url: "<?= site_url('user/ajaxCreateUser'); ?>",
                type: "POST",
                async: false,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                  if(data.status.localeCompare('ERROR') == 0){
                        swal({
                            title: "Gagal Membuat User",
                            text: data.msg,
                            type: "error"
                        });
                   }else if(data.status.localeCompare('WARNING') == 0){
                        swal({
                            title: "Berhasil Membuat User (ada catatan)",
                            text: data.msg,
                            type: "warning"
                        }, function(){
                            $('#declineMessage').val('');
                            reloadDatatable();
                        });
                   }else{
                        swal({
                            title: "Berhasil Membuat User",
                            text: data.msg,
                            type: "success"
                        }, function(){
                            $('#declineMessage').val('');
                            reloadDatatable();
                        });
                  }
                },
                error:function(){
                  swal({
                        title: "Gagal Membuat User",
                        text: "Koneksi atau format data dari server bermasalah. Hubungi Admin",
                        type: "error"
                    });
                }
            });
            $('#addUser').modal('hide');
            return false;
        };
    
        $(document).on('click','.triggerEdit', function (event) {
            $('#editUser').modal('show');
            var id = $(this).attr('data-id');

            $.post($('#site-url').val()+"/user/ajaxGetUser", {
                id:id
            })
            .done(function(data) {
                if(data.status == 'SUCCESS'){
                    $("form#form-edit")[0].reset();
                    $('#id-edit').val(data.data.id);
                    $('#username-edit').val(data.data.username);
                    $('#password-edit').val(data.data.password);
                    $('#name-edit').val(data.data.name);
                    $("#role_id-edit").select2().select2('val',data.data.role_id);
                    $("#spv_id-edit").select2().select2('val',data.data.spv_id);
                }else{
                    swal("Gagal!", data.msg, "error");
                }
            }).fail(function(){
                swal("Gagal!", "Gagal proses, format data dari server tidak valid. Hubungi Admin", "error");
            });

            return false;
        });
        
        function submitUpdate(){
            var formData = new FormData($("form#form-edit")[0]);

            $('#editUser').modal('hide');
            $("form#form-edit")[0].reset();
            $.ajax({
                url: "<?= site_url('user/ajaxUpdateUser'); ?>",
                type: "POST",
                async: false,
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function(data){
                  if(data.status.localeCompare('ERROR') == 0){
                        swal({
                            title: "Gagal Update User",
                            text: data.msg,
                            type: "error"
                        });
                   }else if(data.status.localeCompare('WARNING') == 0){
                        swal({
                            title: "Berhasil Update User (ada catatan)",
                            text: data.msg,
                            type: "warning"
                        }, function(){
                            $('#declineMessage').val('');
                            reloadDatatable();
                        });
                   }else{
                        swal({
                            title: "Berhasil Update User",
                            text: data.msg,
                            type: "success"
                        }, function(){
                            $('#declineMessage').val('');
                            reloadDatatable();
                        });
                  }
                },
                error:function(){
                  swal({
                        title: "Gagal Update",
                        text: "Koneksi atau format data dari server bermasalah. Hubungi Admin",
                        type: "error"
                    });
                }
            });
            $('.modal').modal('hide');
        };
        
        $(document).on('click','.triggerDelete', function () {
            var info = this.id;
            $("#deleteUserModal").modal('show');
            $('#delete').attr('data-id', $(this).attr('data-id'));
        });
        
        $(document).on('click','#delete', function () {
            var id = $('#delete').attr('data-id');
            $.ajax({
                url: "<?= site_url('user/ajaxDeleteUser')?>",
                type: "post",
                dataType: "json",
                async: false,
                data: {
                    id: id
                },
                success: function(data){
                  if(data.status.localeCompare('ERROR') == 0){
                        $("#deleteUserModal").modal('hide');
                        swal({
                            title: "Gagal Delete",
                            text: data.msg,
                            type: "error"
                        });
                   }else if(data.status.localeCompare('WARNING') == 0){
                        swal({
                            title: "Berhasil Hapus (ada catatan)",
                            text: data.msg,
                            type: "warning"
                        }, function(){
                            reloadDatatable();
                        });
                   }else{
                        swal({
                            title: "Berhasil Delete",
                            text: data.msg,
                            type: "success"
                        }, function(){
                            reloadDatatable();
                        });
                  }
                },
                error:function(){
                  swal({
                        title: "Gagal Delete",
                        text: "Koneksi atau format data dari server bermasalah. Hubungi Admin",
                        type: "error"
                    });
                }
            });
            $('.modal').modal('hide');
        });

        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });
        
        $(document).on('click','.triggerAdd', function () { 
            $('#form-add')[0].reset();
        });
    });
    
    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>