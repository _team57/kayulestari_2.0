<script>
    $(document).ready(function() {
        $('#date-filter a').click(function(e) {
            e.preventDefault()
            var href = $(this).attr('href');
            $(this).tab('show');
            var startDate = new Date();
            var endDate = startDate;
            if (href == "#yesterday") {
                startDate = new Date(startDate - (1 * 24 * 3600 * 1000));
                endDate = startDate;
            } else if (href == "#this-week") {
                startDate = new Date(startDate - ((startDate.getDay() - 1) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#previous-week") {
                startDate = new Date(startDate - ((startDate.getDay() + 6) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#this-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            } else if (href == "#previous-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            }
            var startDay = startDate.getDate();
            var startMonth = startDate.getMonth() + 1;
            var startYear = startDate.getFullYear();
            var endDay = endDate.getDate();
            var endMonth = endDate.getMonth() + 1;
            var endYear = endDate.getFullYear();
            if (startDay < 10) {
                startDay = '0' + startDay;
            }
            if (startMonth < 10) {
                startMonth = '0' + startMonth;
            }
            if (endDay < 10) {
                endDay = '0' + endDay;
            }
            if (endMonth < 10) {
                endMonth = '0' + endMonth;
            }
            $('#daterangepicker').val(startMonth + "/" + startDay + "/" + startYear + " - " + endMonth + "/" + endDay + "/" + endYear);
            analysis();
        });
        //Date Pickers
        $('.selectpicker').select2();
        $('#daterangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'MM/DD/YYYY',
            // pick12HourFormat: false
            showDropdowns: true,
            timePicker: false,
        }, function(start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        }).on("apply.daterangepicker", function(ev, picker) {
            analysis();
        });

        analysis();

        $('#shop-filter').on('change', function() {
            analysis();
        });
    });

    function sortNumber(a, b) {
        return a - b;
    }

    function drawNVD3LineChart(targetId, xFormat, seriesData) {
        var xLabel = '';
        if (xFormat == 1) {
            xLabel = "Tanggal";
        } else {
            xLabel = "Jam";
        }
        nv.addGraph(function() {
            var chart = nv.models.lineChart()
                    .x(function(d) {
                        return d[0]
                    })
                    .y(function(d) {
                        return d[1]
                    })
                    .useInteractiveGuideline(true);

            chart.margin().left = 120
            chart.xAxis
                    .tickFormat(function(d) {
                        if (xFormat == 1) {
                            return d3.time.format('%x')(new Date(d))
                        } else {
                            return d
                        }
                    })
                    .axisLabel(xLabel);
            chart.yAxis
                    .tickFormat(function(d) {
                        return "Rp " + d3.format(',.2f')(d)
                    })
            // .axisLabel('Value');

            chart.height(400);
            d3.select('#' + targetId + " svg")
                    .datum(seriesData)
                    .transition().duration(500)
                    .call(chart);


            nv.utils.windowResize(function() {

                chart.update();
                setTimeout(function() {
                    d3.selectAll('#' + targetId + ' .nvd3 circle.nv-point').attr("r", "4");
                }, 300);
            });


            $('#' + targetId).attr('style', "height:400px;");
            $('#' + targetId).data('chart', chart);

            return chart;
        }, function() {

        });
    }

    function drawNVD3MultiBarChart(targetId, seriesData) {
        nv.addGraph(function() {
            var chart = nv.models.multiBarChart()
                    .x(function(d) {
                        return d[0]
                    })
                    .y(function(d) {
                        return d[1]
                    })
                    .reduceXTicks(false)
                    .showControls(false);

            // chart.forceY([10000000]);

            chart.margin().left = 120
            chart.xAxis
                    .showMaxMin(false)
                    ;

            chart.yAxis.tickFormat(function(d) {
                return "Rp " + d3.format(',.2f')(d)
            });


            d3.select('#' + targetId + ' svg')
                    .datum(seriesData)
                    .transition().duration(500)
                    .call(chart)
                    ;
            //        $(".nv-controlsWrap")
            // .attr("transform", "translate(-70,-30)");

            nv.utils.windowResize(function() {
                chart.update();
                setTimeout(function() {
                    d3.selectAll('#' + targetId + ' .nvd3 circle.nv-point').attr("r", "4");
                }, 300);
            });

            $('#' + targetId).attr('style', "height:400px;");
            $('#' + targetId).data('chart', chart);
            return chart;
        }, function() {

        });
    }

    function drawNVD3PieChart(targetId, seriesData, tooltipFormat) {
        nv.addGraph(function() {
            var chart = nv.models.pieChart()
                    .x(function(d) {
                        return d.label
                    })
                    .y(function(d) {
                        return d.value
                    })
                    .showLabels(true);


            chart.labelType("percent");
            chart.height(400);
            chart.width(400);
            chart.tooltipContent(function(key, y, e, graph) {
                var tempFormat = '<p style="margin:0px;font-size:18px;font-weight:bold;">' + key + '</p>'
                        + '<hr style="margin:0px;">';
                if (tooltipFormat == 1) {
                    tempFormat += '<p style="margin:0px;">Rp ' + y + '</p>'
                } else {
                    tempFormat += '<p style="margin:0px;">' + y.replace(".00", ""); + '</p>'
                }
                ;
                return tempFormat;
            });
            // chart.xAxis
            //     .tickFormat(function(d) {
            //         if(xFormat == 1){
            //          return d3.time.format('%x')(new Date(d))
            //         } else {
            //          return d
            //         }
            //     });
            // chart.yAxis.tickFormat(d3.format(',.2f'));

            d3.select('#' + targetId + " svg")
                    .datum(seriesData)
                    .transition().duration(500)
                    .call(chart);

            nv.utils.windowResize(function() {

                chart.update();
                setTimeout(function() {
                    d3.selectAll('#' + targetId + ' .nvd3 circle.nv-point').attr("r", "4");
                }, 300);
            });

            $('#' + targetId).attr('style', "height:400px;");
            $('#' + targetId).data('chart', chart);

            return chart;
        }, function() {

        });
    }

    function analysis() {
        $('#modalLoading').modal({
            backdrop: 'static'
        })
        $('#modalLoading').modal('show');
//        var shop_filter_id = $('select[id=shop-filter]').val();
        var formData = new FormData($("form#form-date")[0]);

        $.ajax({
            method: "POST",
            url: "<?php echo base_url(); ?>index.php/dashboard/analysis/",
            // async:false,
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function(data) {
                $('#modalLoading').modal('hide');
                
                if (!data['total_pohon']) {
                    $('#dashboard-analisis').prop('style', 'display:none;');
                    $('#no-dashboard-analisis').prop('style', 'display:true;');
                } else {
                    $('#dashboard-analisis').prop('style', 'display:true;');
                    $('#no-dashboard-analisis').prop('style', 'display:none;');
                    $('#pohon-terdata').html(data['total_pohon']);
                    $('#lahan-terdata').html(data['total_lahan']);
                }
            },
            error: function() {
                $('#modalLoading').modal('hide');
                swal({
                    title: "Gagal",
                    text: "Koneksi atau format data dari server bermasalah. Hubungi Admin",
                    type: "error"
                });
            }
        });
    }
    
    function addDigitSeparatorWithPoint(value) {
        value = value.toString();
        var minus = "";
        if (value.charAt(0) == '-') {
            minus = "-";
        }
        var values = value.split(".");
        var value1 = values[0];
        var decimal = ',00';
        if(typeof values[1] !== 'undefined'){
            two_digits = values[1].substring(0,2);
            decimal = ','+two_digits;
        }
        return "Rp " + minus + value1
                .replace(/\D/g, "")
                .replace(/\B(?=(\d{3})+(?!\d))/g, ".")+decimal;
    }
    
    function showTooltip(pajakId){
        $('#tooltip-'+pajakId).show();
    }
    
    function hideTooltip(pajakId){
        $('#tooltip-'+pajakId).hide();
    }
</script>