<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <input type="hidden" id="site-url" value="<?= site_url(); ?>">
    <input type="hidden" id="import-type" value="1">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>SOBI - <?php echo $title; ?></p>
                        </li>
                        <li>
                            <a href="<?= site_url(''); ?>" class="active">User List</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg bg-white">
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">
                        User List
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <br>
                    <div class="row">
                        <div class="col-md-2" style="width: 170px;">
                            <button class="btn btn-primary btn-cons triggerAdd">Add User <i class="fs-14 pg-plus"></i></button>
                        </div>
                    </div>
                    <hr>
                    <button style="margin-bottom: 10px" class="btn btn-success btn-cons pull-right triggerRefresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh Table</button>
                    <br>
                    <div>
                        <table class="table table-hover demo-table-search" id="user_table">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Role ID</th>
                                    <th>Role</th>
                                    <th>Spv ID</th>
                                    <th>Spv Name</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade slide-up in" id="addUser" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                            <h5>Add <span class="semi-bold">User</span></h5>
                            <p class="p-b-10">Please fill in the user information detail below</p>
                        </div>
                        <form id="form-add" method="post">
                            <div class="modal-body">
                                <div class="form-group form-group-default required">
                                    <label>Username</label>
                                    <input type="text" name="username" id="username-add" class="form-control" placeholder="Username" required="required"/>
                                </div>
                                <div class="form-group form-group-default required">
                                    <label>Password</label>
                                    <input type="password" name="password" id="password-add" class="form-control" placeholder="Password" required="required"/>
                                </div>
                                <div class="form-group form-group-default required">
                                    <label>Name</label>
                                    <input type="text" name="name" id="name-add" class="form-control" placeholder="name" required="required"/>
                                </div>
                                <div class="form-group">
                                    <label>Role</label>
                                    <select class="selectpicker full-width" name="role_id">
                                        <option value="">- Pilih Role -</option>
                                        <option value="0">Pendata</option>
                                        <option value="1">Supervisor / Verificator</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label>Supervisor</label>
                                    <select class="selectpicker full-width" name="spv_id">
                                        <?php if($spv){ ?>
                                        <option value=""  >- Pilih Supervisor -</option>
                                        <?php }else{ ?>
                                        <option value=""  >- Silahkan Buat Supervisor Terlebih Dahulu -</option>
                                        <?php }
                                        foreach($spv as $supervisor){ ?>
                                            <option value="<?= $supervisor->id ?>"  ><?= $supervisor->spv_name ?></option>
                                        <?php 
                                        } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-white modal-close" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade slide-up in" id="editUser" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="pg-close fs-14"></i>
                            </button>
                            <h5>Edit <span class="semi-bold">User</span></h5>
                            <p class="p-b-10">Please fill in the user information detail below</p>
                        </div>
                        <form id="form-edit" method="post">
                            <div class="modal-body">
                                <div class="modal-body">
                                    <div class="form-group form-group-default required">
                                        <label>Username</label>
                                        <input type="text" name="username" id="username-edit" class="form-control" placeholder="Username" required="required"/>
                                    </div>
                                    <div class="form-group form-group-default required">
                                        <label>Password</label>
                                        <input type="password" name="password" id="password-edit" class="form-control" placeholder="Password" required="required"/>
                                    </div>
                                    <div class="form-group form-group-default required">
                                        <label>Name</label>
                                        <input type="text" name="name" id="name-edit" class="form-control" placeholder="name" required="required"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Role</label>
                                        <select class="selectpicker full-width" name="role_id" id="role_id-edit">
                                            <option value="">- Pilih Role -</option>
                                            <option value="0">Pendata</option>
                                            <option value="1">Supervisor / Verificator</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Supervisor</label>
                                        <select class="selectpicker full-width" name="spv_id" id="spv_id-edit">
                                            <?php if($spv){ ?>
                                            <option value=""  >- Pilih Supervisor -</option>
                                            <?php }else{ ?>
                                            <option value=""  >- Silahkan Buat Supervisor Terlebih Dahulu -</option>
                                            <?php }
                                            foreach($spv as $supervisor){ ?>
                                                <option value="<?= $supervisor->id ?>"  ><?= $supervisor->spv_name ?></option>
                                            <?php 
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" name="id" id="id-edit">
                                <button type="button" class="btn btn-white modal-close" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="deleteUserModal" class="modal fade slide-up disable-scroll in" tabindex="-1" role="dialog" aria-hidden="false">
            <div class="modal-dialog modal-sm">
                <div class="modal-content-wrapper">
                    <div class="modal-content">
                        <div class="modal-header clearfix text-left">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h5 >Delete <span class="semi-bold">Confirmation</span></h5>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure you want to delete this?</p>
                        </div>
                        <form id="form-delete">
                            <div class="modal-footer">
                                <input type="hidden" name="id-delete" id="id-delete">
                                <button type="button" class="btn btn-white modal-close" data-dismiss="modal">No</button>
                                <button type="button" class="btn btn-danger btn-cons" id="delete" data-id="">Yes</button>								
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>