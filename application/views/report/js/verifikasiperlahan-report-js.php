<script type="text/javascript">
    var table;
    function verifikasiPerLahanReportDataTable () {
        var dateString = $('input[id=daterangepicker]').val().split(' - ');
        var type = $('select[id=type-filter]').val();
        if(!type){
            type = 0;
        }
	    table = $('#report_table').addClass('nowrap').DataTable({
            "responsive": {
                details: {
                    type: 'inline',
                    renderer: function (api, rowIdx) {
                        var theRow = api.row(rowIdx);

                        var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                            var header = $(api.column(cell.column).header());

                            return '<tr>' +
                                '<td><b>' +
                                header.text() + ':' +
                                '</b></td> ' +
                                '<td>' +
                                $( api.cell( cell ).node() ).html() +
                                '</td>' +
                                '</tr>';
                        }).toArray().join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/report/ajaxVerifikasiPerLahanReportDataTable/" + dateString[0] + "/" + dateString[1] + "/" + type,
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter:</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['pendata']);
	    		$('td:eq(2)', nRow).html(aData['no_anggota']);
	    		$('td:eq(3)', nRow).html(aData['nama']);
	    		$('td:eq(4)', nRow).html(aData['nama_lahan']);
	    		$('td:eq(5)', nRow).html(aData['assigned']).css("text-align", "right");
	    		$('td:eq(6)', nRow).html(aData['verified_valid']).css("text-align", "right");
	    		$('td:eq(7)', nRow).html(aData['verified_invalid']).css("text-align", "right");
	    		$('td:eq(8)', nRow).html(aData['unverified']).css("text-align", "right");
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "width": "10%", "targets": 0 }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){
        $('#date-filter a').click(function(e) {
            e.preventDefault()
            var href = $(this).attr('href');
            $(this).tab('show');

            var startDate = new Date();
            var endDate = startDate;
            if (href == "#yesterday") {
                startDate = new Date(startDate - (1 * 24 * 3600 * 1000));
                endDate = startDate;
            } else if (href == "#this-week") {
                startDate = new Date(startDate - ((startDate.getDay() - 1) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#previous-week") {
                startDate = new Date(startDate - ((startDate.getDay() + 6) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#this-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            } else if (href == "#previous-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            }
            var startDay = startDate.getDate();
            var startMonth = startDate.getMonth() + 1;
            var startYear = startDate.getFullYear();
            var endDay = endDate.getDate();
            var endMonth = endDate.getMonth() + 1;
            var endYear = endDate.getFullYear();
            if (startDay < 10) {
                startDay = '0' + startDay;
            }
            if (startMonth < 10) {
                startMonth = '0' + startMonth;
            }
            if (endDay < 10) {
                endDay = '0' + endDay;
            }
            if (endMonth < 10) {
                endMonth = '0' + endMonth;
            }
            if (href == "#all") {
                $('#daterangepicker').val("0000-00-00 - 0000-00-00");
            } else{
                $('#daterangepicker').val(startYear + "-" + startMonth + "-" + startDay + " - " + endYear + "-" + endMonth + "-" + endDay);
            }
            setDownloadUrl();
            verifikasiPerLahanReportDataTable();
        });
        
        $('#daterangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'YYYY-MM-DD',
            showDropdowns: true,
            timePicker: false,
            startDate: new Date(),
            endDate: new Date()
        }, function(start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        }).on("apply.daterangepicker", function(ev, picker) {
            setDownloadUrl();
            verifikasiPerLahanReportDataTable();
        });
        
        $('.selectpicker').select2();
        verifikasiPerLahanReportDataTable();
        setDownloadUrl();
        
        $('#type-filter').on('change', function() {
            setDownloadUrl();
            verifikasiPerLahanReportDataTable();
        });
        
        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });

        $(document).on('click','.triggerVerificator', function () { 
            $('#verifListModal').modal('show');
        });
   
        $('#select_all').click(function() {
            $(':checkbox').prop('checked', this.checked);
        });
    });

    function setDownloadUrl() {
        var dateString = $('input[id=daterangepicker]').val().split(' - ');
        var type = $('select[id=type-filter]').val();
        if(!type){
            type = 0;
        }
        
        var href = $(this).attr('href');
        if (href == "#all" || href == '') {
            $('#btn-report-download').attr('href', "<?= site_url('report/downloadVerifikasiPerLahanReport') ?>" + "/0000-00-00/0000-00-00/" + type);
        } else{
            $('#btn-report-download').attr('href', "<?= site_url('report/downloadVerifikasiPerLahanReport') ?>" + "/"  + dateString[0] + "/" + dateString[1] + "/" + type);
        }
    }

    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>