<script type="text/javascript">
    var table;
    function lahanReportDataTable () {
        var pendata = $('select[id=pendata-filter]').val();
	    table = $('#report_table').addClass('nowrap').DataTable({
            "responsive": {
                details: {
                    type: 'inline',
                    renderer: function (api, rowIdx) {
                        var theRow = api.row(rowIdx);

                        var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                            var header = $(api.column(cell.column).header());

                            return '<tr>' +
                                '<td><b>' +
                                header.text() + ':' +
                                '</b></td> ' +
                                '<td>' +
                                $( api.cell( cell ).node() ).html() +
                                '</td>' +
                                '</tr>';
                        }).toArray().join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/report/ajaxLahanReportDataTable/" + pendata,
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter:</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['no_anggota']);
	    		$('td:eq(2)', nRow).html(aData['nama_anggota']);
	    		$('td:eq(3)', nRow).html(aData['nama_lahan']);
	    		$('td:eq(4)', nRow).html(aData['no_sertifikat']);
	    		$('td:eq(5)', nRow).html(aData['jenis_sertifikat']);
	    		$('td:eq(6)', nRow).html(aData['foto_sertifikat']+", "+aData['foto_sketsa']);
	    		$('td:eq(7)', nRow).html(aData['luas_lahan']);
	    		$('td:eq(8)', nRow).html(aData['pendata']);
                $('td:eq(9)', nRow).html("<button data-id="+aData['lahan_id']+" class='btn btn-info btn-sm btn-cons triggerDetail' data-toggle='modal' data-target='#detailLahan'>Detail <i class='fs-14 fa fa-list'></span></button>");
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0,5,9] },
                { "width": "10%", "targets": 0 }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){

        $('.selectpicker').select2();
        setDownloadUrl();
        lahanReportDataTable();
        
        $('#pendata-filter').on('change', function() {
            setDownloadUrl();
            lahanReportDataTable();
        });
        
        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });
        
        $(document).on('click','.triggerDetail', function (event) {
            var modal_body = document.getElementById('modal_body');
            modal_body.innerHTML = "";
            var no_anggota = $(this).attr('data-no_anggota');

            $.post($('#site-url').val()+"/anggota/ajaxGetAnggota", {
                no_anggota:no_anggota
            })
            .done(function(data) {
               if(data.status == 'SUCCESS'){
                    var detailAnggota = data.data;
                    var modal_body = document.getElementById('modal_body');
                    modal_body.innerHTML = "";
                    
                    $.each(detailAnggota.profile, function(key, value) {
                        var tr_detail = document.createElement('tr');
                        var td_key = document.createElement('td');
                        td_key.textContent = key;
                        var td_value = document.createElement('td');
                        td_value.textContent = value;
                        td_value.style.fontWeight = 'bold';
                        td_value.style.paddingLeft = '5px';

                        tr_detail.appendChild(td_key);
                        tr_detail.appendChild(td_value);
                        modal_body.appendChild(tr_detail);
                    });
                    
                    $("#detailLahan").modal('show');
                }else{
                    swal("Gagal!", data.msg, "error");
                }
            }).fail(function(){
                swal("Gagal!", "Gagal proses, format data dari server tidak valid. Hubungi Admin", "error");
            });

            return false;
        });
    });

    function setDownloadUrl() {
        var pendata = $('select[id=pendata-filter]').val();
        $('#btn-report-download').attr('href', "<?= site_url('report/downloadLahanReport') ?>" + "/" + pendata);
    }

    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>