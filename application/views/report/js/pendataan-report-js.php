<script type="text/javascript">
    var table;
    function pendataanReportDataTable () {
        var dateString = $('input[id=daterangepicker]').val().split(' - ');
        var pendata = $('select[id=pendata-filter]').val();
	    table = $('#report_table').addClass('nowrap').DataTable({
            "responsive": {
                details: {
                    type: 'inline',
                    renderer: function (api, rowIdx) {
                        var theRow = api.row(rowIdx);

                        var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                            var header = $(api.column(cell.column).header());

                            return '<tr>' +
                                '<td><b>' +
                                header.text() + ':' +
                                '</b></td> ' +
                                '<td>' +
                                $( api.cell( cell ).node() ).html() +
                                '</td>' +
                                '</tr>';
                        }).toArray().join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/report/ajaxPendataanReportDataTable/" + dateString[0] + "/" + dateString[1] + "/" + pendata,
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter:</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['pendata']);
	    		$('td:eq(2)', nRow).html(aData['tanggal_pendataan']);
	    		$('td:eq(3)', nRow).html(aData['no_anggota']);
	    		$('td:eq(4)', nRow).html(aData['nama_anggota']);
	    		$('td:eq(5)', nRow).html(aData['nama_lahan']);
	    		$('td:eq(6)', nRow).html(aData['awal_pendataan']);
	    		$('td:eq(7)', nRow).html(aData['akhir_pendataan']);
	    		$('td:eq(8)', nRow).html(aData['jumlah_pohon']);
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "width": "10%", "targets": 0 }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){
        $('#date-filter a').click(function(e) {
            e.preventDefault()
            var href = $(this).attr('href');
            $(this).tab('show');

            var startDate = new Date();
            var endDate = startDate;
            if (href == "#yesterday") {
                startDate = new Date(startDate - (1 * 24 * 3600 * 1000));
                endDate = startDate;
            } else if (href == "#this-week") {
                startDate = new Date(startDate - ((startDate.getDay() - 1) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#previous-week") {
                startDate = new Date(startDate - ((startDate.getDay() + 6) * 24 * 3600 * 1000));
                endDate = new Date(startDate - (-6 * 24 * 3600 * 1000));
            } else if (href == "#this-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth(), 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            } else if (href == "#previous-month") {
                startDate = new Date(startDate.getFullYear(), startDate.getMonth() - 1, 1);
                endDate = new Date(startDate.getFullYear(), startDate.getMonth() + 1, 0);
            }
            var startDay = startDate.getDate();
            var startMonth = startDate.getMonth() + 1;
            var startYear = startDate.getFullYear();
            var endDay = endDate.getDate();
            var endMonth = endDate.getMonth() + 1;
            var endYear = endDate.getFullYear();
            if (startDay < 10) {
                startDay = '0' + startDay;
            }
            if (startMonth < 10) {
                startMonth = '0' + startMonth;
            }
            if (endDay < 10) {
                endDay = '0' + endDay;
            }
            if (endMonth < 10) {
                endMonth = '0' + endMonth;
            }
            $('#daterangepicker').val(startYear + "-" + startMonth + "-" + startDay + " - " + endYear + "-" + endMonth + "-" + endDay);
            setDownloadUrl();
            pendataanReportDataTable();
        });
        
        $('#daterangepicker').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            format: 'YYYY-MM-DD',
            showDropdowns: true,
            timePicker: false,
        }, function(start, end, label) {
            // console.log(start.toISOString(), end.toISOString(), label);
        }).on("apply.daterangepicker", function(ev, picker) {
            setDownloadUrl();
            pendataanReportDataTable();
        });
        
        $('.selectpicker').select2();
        setDownloadUrl();
        pendataanReportDataTable();
        
        $('#pendata-filter').on('change', function() {
            setDownloadUrl();
            pendataanReportDataTable();
        });
        
        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });
    });

    function setDownloadUrl() {
        var dateString = $('input[id=daterangepicker]').val().split(' - ');
        var pendata = $('select[id=pendata-filter]').val();
        $('#btn-report-download').attr('href', "<?= site_url('report/downloadPendataanReport') ?>" + "/" + dateString[0] + "/" + dateString[1] + "/" + pendata);
    }

    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>