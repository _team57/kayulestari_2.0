<script type="text/javascript">
    var table;
    function fotoLahanReportDataTable () {
	    table = $('#report_table').addClass('nowrap').DataTable({
            "responsive": {
                details: {
                    type: 'inline',
                    renderer: function (api, rowIdx) {
                        var theRow = api.row(rowIdx);

                        var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                            var header = $(api.column(cell.column).header());

                            return '<tr>' +
                                '<td><b>' +
                                header.text() + ':' +
                                '</b></td> ' +
                                '<td>' +
                                $( api.cell( cell ).node() ).html() +
                                '</td>' +
                                '</tr>';
                        }).toArray().join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/report/ajaxFotoLahanReportDataTable/",
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter:</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['no_anggota']);
	    		$('td:eq(2)', nRow).html(aData['nama']);
	    		$('td:eq(3)', nRow).html(aData['nama_lahan']);
	    		$('td:eq(4)', nRow).html(aData['foto_sertifikat']);
	    		$('td:eq(5)', nRow).html(aData['jumlah_foto_sertifikat']).css("text-align", "right");
	    		$('td:eq(6)', nRow).html(aData['foto_sketsa']);
	    		$('td:eq(7)', nRow).html(aData['jumlah_foto_sketsa']).css("text-align", "right");
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0,4,5,6,7] },
                { "width": "10%", "targets": 0 }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){
    
        fotoLahanReportDataTable();
        
        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });
    });

    function setDownloadUrl() {
        $('#btn-report-download').attr('href', "<?= site_url('report/downloadFotoLahanReport/') ?>");
    }

    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>