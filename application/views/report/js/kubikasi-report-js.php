<script type="text/javascript">
    var table;
    function kubikasiReportDataTable () {
	    table = $('#kubikasi_report_table').addClass('nowrap').DataTable({
            "responsive": {
                details: {
                    type: 'inline',
                    renderer: function (api, rowIdx) {
                        var theRow = api.row(rowIdx);

                        var data = api.cells(rowIdx, ':hidden').eq(0).map(function (cell) {
                            var header = $(api.column(cell.column).header());

                            return '<tr>' +
                                '<td><b>' +
                                header.text() + ':' +
                                '</b></td> ' +
                                '<td>' +
                                $( api.cell( cell ).node() ).html() +
                                '</td>' +
                                '</tr>';
                        }).toArray().join('');

                        return data ?
                            $('<table/>').append(data) :
                            false;
                    }
                }
            },
            "stateSave": true,
	    	"processing" : true,
	    	"serverSide" : true,
            "bDestroy": true,
	    	"ajax" : "<?php echo base_url();?>index.php/report/ajaxKubikasiReportDataTable/",
	    	"columns" : [
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null},
	    		{'data' : null}
	    	],
	    	"bJQueryUI": false,
           	"bAutoWidth": false,
	    	"sPaginationType" : "full_numbers",
           	"sDom": '<"datatable-header"fl>t<"datatable-footer"ip>',
           	"oLanguage": {
                "sSearch": "<span>Filter (jenis pohon):</span> _INPUT_",
                "sLengthMenu": "<span>Show entries:</span> _MENU_",
                "oPaginate": {
                    "sFirst": "<<", 
                    "sLast": ">>", 
                    "sNext": ">", 
                    "sPrevious": "<"
                }
            },
	    	"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
	    		$('td:eq(0)', nRow).html(aData['resultIndex']);
	    		$('td:eq(1)', nRow).html(aData['jenis_pohon']);
	    		$('td:eq(2)', nRow).html(aData['kelas_diameter']);
	    		$('td:eq(3)', nRow).html(aData['jumlah_pohon']).css("text-align", "center");
	    		$('td:eq(4)', nRow).html(aData['total_volume']).css("text-align", "right");
	    		$('td:eq(5)', nRow).html(aData['average_volume']).css("text-align", "right");
	    	},
            "aoColumnDefs": [
                { "bSortable": false, "aTargets": [0] },
                { "width": "10%", "targets": 0 }
            ],
	    	"aaSorting": []
	    });
    }
    
    $(document).ready(function (){
        
        kubikasiReportDataTable();
        
        $(document).on('click','.triggerRefresh', function () { 
            reloadDatatable();
        });
    });

    function reloadDatatable() {
        table.ajax.reload();
    }
   
</script>