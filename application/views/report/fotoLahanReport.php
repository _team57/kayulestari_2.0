<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <input type="hidden" id="site-url" value="<?= site_url(); ?>">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>SOBI - <?php echo $title; ?></p>
                        </li>
                        <li><a href="<?= site_url(''); ?>" class="active">Foto Lahan Report</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg bg-white">
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Foto Lahan Report
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <!-- Start Panel -->
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <a href="<?= site_url("report/downloadFotoLahanReport/"); ?>" class="btn btn-default btn-report-download" style="float:right;" id="btn-report-download"><i class="fa fa-download"></i> Download to Excel</a>
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!-- End Panel -->
                    </div>
                    <br>
                    <div id="report-list">
                        <button style="margin-bottom: 10px" class="btn btn-success btn-cons pull-right triggerRefresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh Table</button>
                        <br>
                        <table class="table table-hover demo-table-search" id="report_table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>No Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Nama Lahan</th>
                                    <th>Foto Sertifikat</th>
                                    <th>Jumlah</th>
                                    <th>Foto Sketsa</th>
                                    <th>Jumlah</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>