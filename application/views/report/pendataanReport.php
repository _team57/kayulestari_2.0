<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <input type="hidden" id="site-url" value="<?= site_url(); ?>">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>SOBI - <?php echo $title; ?></p>
                        </li>
                        <li><a href="<?= site_url(''); ?>" class="active">Pendataan Report</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg bg-white">
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Pendataan Report
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <!-- Start Panel -->
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-pills nav-custom" role="tablist" id="date-filter">
                                        <li role="presentation" class="active"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">Hari Ini</a></li>
                                        <li role="presentation"><a href="#yesterday" aria-controls="yesterday" role="tab" data-toggle="tab">Kemarin</a></li>
                                        <li role="presentation"><a href="#this-week" aria-controls="this-week" role="tab" data-toggle="tab">Pekan Ini</a></li>
                                        <li role="presentation"><a href="#previous-week" aria-controls="previous-week" role="tab" data-toggle="tab">Pekan Lalu</a></li>
                                        <li role="presentation"><a href="#this-month" aria-controls="this-month" role="tab" data-toggle="tab">Bulan Ini</a></li>
                                        <li role="presentation"><a href="#previous-month" aria-controls="previous-month" role="tab" data-toggle="tab">Bulan Lalu</a></li>
                                        <li role="presentation"><a href="#date-range" aria-controls="date-range" role="tab" data-toggle="tab">Rentang Waktu</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="today"></div>
                                        <div role="tabpanel" class="tab-pane" id="yesterday"></div>
                                        <div role="tabpanel" class="tab-pane" id="this-week"></div>
                                        <div role="tabpanel" class="tab-pane" id="previous-week"></div>
                                        <div role="tabpanel" class="tab-pane" id="this-month"></div>
                                        <div role="tabpanel" class="tab-pane" id="previous-month"></div>
                                        <div role="tabpanel" class="tab-pane" id="date-range">
                                            <form id="form-date" class="col-md-4">
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                    <input type="text" style="width: 100%" name="daterangepicker" id="daterangepicker" class="form-control" value="<?php echo date('Y-m-d', strtotime(date('Y-m-d')) - 86400 * 0); ?> - <?php echo date('Y-m-d'); ?>" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <a href="<?= site_url("report/downloadPendataanReport/"); ?>" class="btn btn-default btn-report-download" style="float:right;" id="btn-report-download"><i class="fa fa-download"></i> Download to Excel</a>
                                    <br>
                                    <div id="filter-pendata-tab" class="col-md-6">
                                        <label>Pilih Pendata</label>
                                        <select id="pendata-filter" class="selectpicker">
                                            <option value="0">Semua Pendata</option>
                                            <?php foreach ($pendata as $user) {?>
                                            <option value="<?php echo $user->id; ?>"><?php echo $user->pendata_name; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Panel -->
                    </div>
                    <br>
                    <div id="report-list">
                        <button style="margin-bottom: 10px" class="btn btn-success btn-cons pull-right triggerRefresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh Table</button>
                        <br>
                        <table class="table table-hover demo-table-search" id="report_table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama Pendata</th>
                                    <th>Tanggal Pendataan</th>
                                    <th>No Anggota</th>
                                    <th>Nama Anggota</th>
                                    <th>Nama Lahan</th>
                                    <th>Awal Pendataan</th>
                                    <th>Akhir Pendataan</th>
                                    <th>Jumlah Pohon</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>