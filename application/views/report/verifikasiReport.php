<!-- START PAGE CONTENT WRAPPER -->
<div class="page-content-wrapper">
    <input type="hidden" id="site-url" value="<?= site_url(); ?>">
    <!-- START PAGE CONTENT -->
    <div class="content">
        <!-- START JUMBOTRON -->
        <div class="jumbotron" data-pages="parallax">
            <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="inner">
                    <!-- START BREADCRUMB -->
                    <ul class="breadcrumb">
                        <li>
                            <p>SOBI - <?php echo $title; ?></p>
                        </li>
                        <li><a href="<?= site_url(''); ?>" class="active">Verification Report</a>
                        </li>
                    </ul>
                    <!-- END BREADCRUMB -->
                </div>
            </div>
        </div>
        <!-- END JUMBOTRON -->
        <div class="container-fluid container-fixed-lg bg-white">
            <div class="panel panel-transparent">
                <div class="panel-heading">
                    <div class="panel-title">Verification Report
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <!-- Start Panel -->
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <ul class="nav nav-pills nav-custom" role="tablist" id="date-filter">
                                        <li role="presentation" class="active"><a href="#all" aria-controls="all" role="tab" data-toggle="tab">Semua</a></li>
                                        <li role="presentation"><a href="#today" aria-controls="today" role="tab" data-toggle="tab">Hari Ini</a></li>
                                        <li role="presentation"><a href="#yesterday" aria-controls="yesterday" role="tab" data-toggle="tab">Kemarin</a></li>
                                        <li role="presentation"><a href="#this-week" aria-controls="this-week" role="tab" data-toggle="tab">Pekan Ini</a></li>
                                        <li role="presentation"><a href="#previous-week" aria-controls="previous-week" role="tab" data-toggle="tab">Pekan Lalu</a></li>
                                        <li role="presentation"><a href="#this-month" aria-controls="this-month" role="tab" data-toggle="tab">Bulan Ini</a></li>
                                        <li role="presentation"><a href="#previous-month" aria-controls="previous-month" role="tab" data-toggle="tab">Bulan Lalu</a></li>
                                        <li role="presentation"><a href="#date-range" aria-controls="date-range" role="tab" data-toggle="tab">Rentang Waktu</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="all"></div>
                                        <div role="tabpanel" class="tab-pane" id="today"></div>
                                        <div role="tabpanel" class="tab-pane" id="yesterday"></div>
                                        <div role="tabpanel" class="tab-pane" id="this-week"></div>
                                        <div role="tabpanel" class="tab-pane" id="previous-week"></div>
                                        <div role="tabpanel" class="tab-pane" id="this-month"></div>
                                        <div role="tabpanel" class="tab-pane" id="previous-month"></div>
                                        <div role="tabpanel" class="tab-pane" id="date-range">
                                            <form id="form-date" class="col-md-4">
                                                <div class="input-prepend input-group">
                                                    <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                                    <input type="text" style="width: 100%" name="daterangepicker" id="daterangepicker" class="form-control" value="0000-00-00 - 0000-00-00" />
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <a href="<?= site_url("report/downloadVerifikasiReport/0000-00-00/0000-00-00"); ?>" class="btn btn-default btn-report-download" style="float:right;" id="btn-report-download"><i class="fa fa-download"></i> Download to Excel</a>
                                    <br>
                                    <?php 
//                                        if(stristr($title, 'kayulestari_kwlm_prod') !== FALSE){ ?>
<!--                                            <div id="filter-type-tab" class="col-md-6">
                                                <label>Pilih Type</label>
                                                <select id="type-filter" class="selectpicker">
                                                    <option value="0">None</option>
                                                    <option value="1">1-15 Januari</option>
                                                    <option value="2">16-31 Januari</option>  
                                                </select>
                                            </div>--><?php
//                                        } 
                                    ?>
                                </div>
                            </div>
                        </div>
                        <!-- End Panel -->
                    </div>
                    <br>
                    <div id="report-list">
                        <button style="margin-bottom: 10px" class="btn btn-success btn-cons pull-right triggerRefresh"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh Table</button>
                        <br>
                        <table class="table table-hover demo-table-search" id="verifikasi_report_table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Nama</th>
                                    <th>Username</th>
                                    <th>Jumlah Lahan</th>
                                    <th>Assigned</th>
                                    <th>Verified (Valid)</th>
                                    <th>Verified (Invalid)</th>
                                    <th>Unverified</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Detail -->
<div class="modal fade slide-up in" id="verifListModal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-wrapper">
            <div class="modal-content">
                <div class="modal-header clearfix text-left">
                    <h5>List <span class="semi-bold">Verificator</span></h5>
                    <hr>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <?php foreach($verif as $verificator){ ?>
                            <div class="col-md-4">
                                <div class="checkbox check-primary">
                                    <input value="<?= $verificator->id ?>" id="<?= $verificator->id ?>" type="checkbox" >
                                    <label for="<?= $verificator->id ?>"><?= $verificator->name ?></label>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4">
                            <div class="checkbox check-primary">
                                <input id="select_all" type="checkbox">
                                <label for="select_all">Select All</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white modal-close" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Apply</button>
                </div>
            </div>
        </div>
    </div>
</div>