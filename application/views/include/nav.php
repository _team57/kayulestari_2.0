  <body class="fixed-header">
    <!-- BEGIN SIDEBAR -->
    <div class="page-sidebar" data-pages="sidebar">
      <div id="appMenu" class="sidebar-overlay-slide from-top">
      </div>
      <!-- BEGIN SIDEBAR HEADER -->
      <div class="sidebar-header">
        <img src="<?= base_url() ?>resources/template/assets/img/logo_white.png" alt="logo" class="brand" data-src="<?= base_url() ?>resources/template/assets/img/logo_white.png" data-src-retina="<?= base_url() ?>resources/template/assets/img/logo_white_2x.png" width="78" height="22">
        <div class="sidebar-header-controls">
          <!-- <button data-pages-toggle="#appMenu" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" type="button"><i class="fa fa-angle-down fs-16"></i>
          </button> -->
          <button data-toggle-pin="sidebar" class="btn btn-link visible-lg-inline" type="button"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR HEADER -->
      <!-- BEGIN SIDEBAR MENU -->
      <div class="sidebar-menu">
        <ul class="menu-items">
            <li class="">
              <a href="<?= site_url('dashboard') ?>" class="detailed">
                <span class="title">Dashboard</span>
              </a>
              <span class="icon-thumbnail "><i class="fs-14 fa fa-area-chart"></i></span>
            </li><li class="">
              <a href="<?= site_url('user') ?>" class="detailed">
                <span class="title">User</span>
              </a>
              <span class="icon-thumbnail "><i class="fs-14 fa fa-user"></i></span>
            </li>
            <li class="">
              <a href="<?= site_url('anggota') ?>" class="detailed">
                <span class="title">Anggota</span>
              </a>
              <span class="icon-thumbnail "><i class="fs-14 fa fa-child"></i></span>
            </li>
            <li class="">
              <a href="<?= site_url('lahan') ?>" class="detailed">
                <span class="title">Lahan</span>
              </a>
              <span class="icon-thumbnail "><i class="fs-14 fa fa-picture-o"></i></span>
            </li>
            <li class="">
              <a href="<?= site_url('pohon') ?>" class="detailed">
                <span class="title">Pohon</span>
              </a>
              <span class="icon-thumbnail "><i class="fs-14 fa fa-tree"></i></span>
            </li>
            <li class="">
              <a href="javascript:;" class="detailed">
                <span class="title">Laporan</span>
                <span class="arrow"></span>
              </a>
              <span class="icon-thumbnail"><i class="fs-14 fa fa-list-alt"></i></span>
              <ul class="sub-menu">
                <li class="">
                  <a href="<?= site_url('report/pendataanReport') ?>">Laporan Pedataan</a>
                  <span class="icon-thumbnail">Lp</span>
                </li>
                <li class="">
                  <a href="<?= site_url('report/lahanReport') ?>">Laporan Lahan</a>
                  <span class="icon-thumbnail">Lh</span>
                </li>
                <li class="">
                  <a href="<?= site_url('report/verifikasiReport') ?>">Laporan Verifikasi</a>
                  <span class="icon-thumbnail">Lv</span>
                </li>
                <?php 
                    if(stristr($title, 'kwlm') !== FALSE) { 
                ?>
                <li class="">
                  <a href="<?= site_url('report/verifikasiPerLahanReport') ?>">Laporan Verifikasi Per Lahan</a>
                  <span class="icon-thumbnail">Lvl</span>
                </li>
                <?php
                    }
                ?>
                <li class="">
                  <a href="<?= site_url('report/fotoLahanReport') ?>">Laporan Foto Lahan</a>
                  <span class="icon-thumbnail">Lfl</span>
                </li>
                <li class="">
                  <a href="<?= site_url('report/kubikasiReport') ?>">Laporan Kubikasi</a>
                  <span class="icon-thumbnail">Lk</span>
                </li>
              </ul>
            </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
    <!-- START PAGE-CONTAINER -->
    <div class="page-container">
      <!-- START PAGE HEADER WRAPPER -->
      <!-- START HEADER -->
      <div class="header ">
        <!-- START MOBILE CONTROLS -->
        <!-- LEFT SIDE -->
        <div class="pull-left full-height visible-sm visible-xs">
          <!-- START ACTION BAR -->
          <div class="sm-action-bar">
            <a href="#" class="btn-link toggle-sidebar" data-toggle="sidebar">
              <span class="icon-set menu-hambuger"></span>
            </a>
          </div>
          <!-- END ACTION BAR -->
        </div>
        <!-- END MOBILE CONTROLS -->
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
              <img src="<?= base_url() ?>resources/template/assets/img/logo.png" alt="logo" data-src="<?= base_url() ?>resources/template/assets/img/logo.png" data-src-retina="<?= base_url() ?>resources/template/assets/img/logo_2x.png" width="78" height="22">
            </div>
          </div>
        </div>
        <div class="pull-right full-height">
          <!-- START User Info-->
          <div class="m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
              <span class="semi-bold">Hello</span> 
              <span class="text-master">
                <?php 
                  if ($this->session->userdata('admin_logged_in')){
                    echo $this->session->userdata['admin_logged_in']['username'];
                  } elseif ($this->session->userdata('logged_in')) {
                    echo $this->session->userdata['logged_in']['name'];
                  } else {
                    echo 'User';
                  }
                ?>
              </span>
            </div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5">
                <?php
                    if($this->session->userdata['logged_in']['userAvatar'] == '' || $this->session->userdata['logged_in']['userAvatar'] == null){
                        $avatar = 'default-avatar.jpg';
                    }else{
                        $avatar = $this->session->userdata['logged_in']['userAvatar'];
                    }
                ?>
                <?php if($this->config->item('use_amazon_server')) { ?>
                  <img src="<?= $this->config->item('amazon_url').$this->config->item('folder_name').$avatar ?>" alt="" width="32" height="32">
                <?php } else { ?>
                  <img src="<?= $base_url().$this->config->item('folder_name').$avatar ?>" alt="" width="32" height="32">
                <?php } ?>
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown" role="menu">
                <li class="bg-master-lighter">
                  <a href="<?php if($this->session->userdata('admin_logged_in')){ echo site_url('admin/logout'); } else {echo site_url('auth/logout');} ?>" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->
      <!-- END PAGE HEADER WRAPPER -->