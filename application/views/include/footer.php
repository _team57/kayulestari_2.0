<!-- START FOOTER -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright © 2016</span>
              <span class="font-montserrat">SOBI</span>.
              <span class="hint-text">All rights reserved.</span>
              <!--v1.0.10-->
              <!-- <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a>
                        </span> -->
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
              <a href="#">Hand-crafted</a>
              <span class="hint-text">&amp; Made with Love ®</span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END FOOTER -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->
  </div>
    <!-- BEGIN VENDOR JS -->
    <script src="<?= base_url() ?>resources/template/assets/plugins/pace/pace.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/boostrapv3/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script src="<?= base_url("resources/js/sweet-alert/sweet-alert.min.js"); ?>"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/lib/d3.v3.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/nv.d3.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/utils.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/tooltip.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/interactiveLayer.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/models/axis.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/bootstrap-tag/bootstrap-tagsinput.js"></script>
	<script src="<?= base_url() ?>resources/js/nested-sortable/jquery.mjs.nestedSortable.js"></script>
    <!-- Begin Auto Complete Form -->
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-autocomplete/jquery-ui.js"></script>
    <!-- End Auto Complete Form -->
    <?php if(isset($useDatatable) && $useDatatable){ ?>
    <!-- BEGIN DATATABLE JS -->
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/classie/classie.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/datatables-responsive/js/dataTables.responsive_2.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/js/moment/moment.min.js"></script>
    <script type="text/javascript" src="<?= base_url();?>resources/js/date-range-picker/daterangepicker.js"></script>
    <!-- END DATATABLE JS -->
    <?php } ?>
    <script src="<?= base_url() ?>resources/template/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/moment/moment.min.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <script type="text/javascript" src="<?= base_url() ?>resources/template/assets/plugins/timepicker/timepicker.js"></script>
    <script src="<?= base_url() ?>resources/template/assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?= base_url();?>resources/js/highcharts/highcharts.js"></script>
    <script src="<?= base_url();?>resources/js/highcharts/exporting.js"></script>
    <?php if(isset($useDropzoneJs) && $useDropzoneJs){ ?>
    <!-- BEGIN DropzoneJs JS-->
    <script src="<?= base_url() ?>resources/template/assets/plugins/dropzone/dropzone.min.js" type="text/javascript"></script>
    <!-- END DropzoneJs JS-->
    <?php } ?>
    <script src="<?= base_url('resources/js/validation/jquery.form-validator.min.js'); ?>"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?= base_url() ?>resources/template/pages/js/pages.js" type="text/javascript"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="<?= base_url() ?>resources/template/assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->
    <?php if(isset($extendFooter) && $extendFooter) { echo $extendFooter; } ?>
    
  </body>
</html>