var i18n = $.extend({}, i18n || {}, {
    datepicker: {
        dateformat: {
            "fulldayvalue": "d/M/yyyy",
            "separator": "/",
            "year_index": 2,
            "month_index": 0,
            "day_index": 1,
            "sun": "Min",
            "mon": "Sen",
            "tue": "Sel",
            "wed": "Rab",
            "thu": "Kam",
            "fri": "Jum",
            "sat": "Sab",
            "jan": "Januari",
            "feb": "Februari",
            "mar": "Maret",
            "apr": "April",
            "may": "Mei",
            "jun": "Juni",
            "jul": "Juli",
            "aug": "Agustus",
            "sep": "September",
            "oct": "Oktober",
            "nov": "November",
            "dec": "Desember",
            "postfix": ""
        },
        ok: " Ok ",
        cancel: "Batal",
        today: "Hari ini",
        prev_month_title: "bulan sebelumnya",
        next_month_title: "bulan selanjutnya"
    }
});

