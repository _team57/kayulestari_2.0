<style>
    /*.footer{
        padding-top: 60px;
        padding-left: 0;
        padding-right: 0;
        padding-bottom: 0;
        background: none repeat scroll 0% 0% #333;
        color: #fff;

    }

    .footer-top{
        padding-left: 100px;
        padding-right: 80px;
    }

    .footer-top h3{
        color: #fff;
        text-transform: uppercase;
        font-weight: bold;
    }

    .footer-top p{
        font-size: 16px;
    }
    .footer-bottom{
        border-top: 1px solid rgba(255, 255, 255, 0.07);
        background: none repeat scroll 0% 0% #3A3A3A;
        padding: 18px 30px;
        color: #808080;
        border-bottom: 5px solid #808080;
        margin-top: 60px;
        font-size: 14px;
        text-transform: uppercase;
        font-weight: 600;
        padding-left: 100px;
    }

    .fa-footer{
        font-size: 30px;
        color: #808080;
    }
    .fa-footer strong{
        margin-left: 30px;
    }

    .footer-about{
        margin-top: 20px;
        line-height: 1.8em;
font-family: Open Sans;
font-size: 13px;
 font-weight: 800 !important;
    }

    @media screen and (max-width : 980px){
        .footer-top h3{
            margin-top: 30px !important;
        }
    }*/

footer {
    position: fixed;
    left: 0;
    bottom: 0;
    height: 100px;
    width: 100%;
    background: #333;
}
</style>
<!-- Start Footer -->
<footer>
    <!--<div class="footer-top">
        <div class="col-md-4">
            <h3> Tentang Company </h3>
            <p> PT Company adalah perusahaan Teknologi Informasi. </p>
            <div class="row footer-about">
                <div class="col-md-1 col-sm-1">
                    <i class="fa fa-envelope-o fa-footer"></i>
                </div>
                <div class="col-md-9 col-sm-9">
                    admin@innolabs.id
                </div>
            </div>
            <div class="row footer-about">
                <div class="col-md-1 col-sm-1">
                    <i class="fa fa-phone fa-footer"></i>
                </div>
                <div class="col-md-9 col-sm-9">
                    +6221xxxxxxx <br/> +6221xxxxxxx
                </div>
            </div>
            <div class="row footer-about">
                <div class="col-md-1 col-sm-1">
                    <i class="fa fa-map-marker fa-footer"></i>
                </div>
                <div class="col-md-9 col-sm-9">
                    Headquarters <br> Company Technology Pte.Ltd<br>123 Lower Cross Street #11<br>Chinatown<br>USA, 112233
                </div>
            </div>
            <div class="row footer-about">
                <div class="col-md-1 col-sm-1">
                    <i class="fa fa-map-marker fa-footer"></i>
                </div>
                <div class="col-md-9 col-sm-9">
                   Sales Office <br> Alfath Tower Floor,<br/> Jl. Margonda Raya, No. 122 Jakarta Selatan,<br/> 12490 Indonesia
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h3> Latest Tweet </h3>
            <?php if (base_url() == 'http://innolabs.id' || base_url() == 'http://innolabs.id/en') { ?>

                <a class="twitter-timeline" data-widget-id="600720083413962752" href="https://twitter.com/CompanyTech" data-screen-name="CompanyTech">Tweets by @CompanyTech </a>
                <script>!function(d,s,id){
                    var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';
                    if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);
                    }
                }
                (document,"script","twitter-wjs");
                </script> 
            <?php } else { ?>
                <p>Anda sedang tidak terkoneksi dengan internet</p>
            <?php } ?>
        </div>
        <div class="col-md-4">
            <h3> Facebook and Fan Page </h3>
        </div>
    </div>  
    <div class="col-md-12 footer-bottom">
        Copyright © 2015 <a href="http://themeforest.net/user/egemem/portfolio" target="_blank"></a> All rights reserved.
    </div>
</div>-->
<!-- End Footer -->
</footer>
<!-- End Content -->
<!-- //////////////////////////////////////////////////////////////////////////// --> 
</body>
</html>