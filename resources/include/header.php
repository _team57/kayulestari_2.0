<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
                <title>Jagoer Administration System</title>
        <!--<meta name="description" content="Selamat datang di form pendaftaran karyawan">
        //<meta name="keywords" content="karir, karyawan, karyawan baru, lowongan, teknologi informasi" />
        //<link rel="shortcut icon" type="image/x-icon" href="<?= base_url("resources/img/favicon-1.ico"); ?>">-->


        <link href="<?= base_url("resources/css/root.css"); ?>" rel="stylesheet">
        <!-- <javascript> -->

        <!-- ================================================
        jQuery Library
        ================================================ -->
        <script type="text/javascript" src="<?= base_url("resources/js/jquery.min.js"); ?>"></script>

        <!-- ================================================
        Bootstrap Core JavaScript File
        ================================================ -->
        <script src="<?= base_url("resources/js/bootstrap/bootstrap.min.js"); ?>"></script>

        <!-- ================================================
        Plugin.js - Some Specific JS codes for Plugin Settings
        ================================================ -->
        <script type="text/javascript" src="<?= base_url("resources/js/plugins.js"); ?>"></script>

        <!-- ================================================
        Data Tables
        ================================================ -->
        <script src="<?= base_url("resources/js/datatables/datatables.min.js"); ?>"></script>
        <script src="<?php echo base_url(); ?>uikit/js/uikit.min.js"></script>
        <link rel="stylesheet" href="<?php echo base_url(); ?>uikit/css/uikit.almost-flat.css" />
        
        <!-- ================================================
        Bootstrap Select
        ================================================ -->
        <script type="text/javascript" src="<?= base_url("resources/js/bootstrap-select/bootstrap-select.js"); ?>"></script>
    </head>

    <body>
        <style>
            .body{
                text-shadow: none;
            }
            #top{
                text-shadow: none;
                padding-left: 25px;
                padding-right: 25px;
            }

            .top-right a{
                padding-left: 10px;
                /*padding-right: 10px;*/
                font-family: "Montserrat";
                font-size: 13px;
                text-transform: uppercase;
                color: #37363E;
                text-align: center;
                display: inline-block;
                font-weight: 500;
            }

            .top-right a:after{
                content: '';
                display: block;
                height: 2px;
                width: 0;
                background: transparent;
                transition: width .5s ease, background-color .5s ease;
            }

            .top-right a:hover{
                color: #37363E;
                box-shadow: none;
                text-decoration: none;
            }
            .top-right a:hover:after{
                /*border-bottom: 3px solid #0087B4;
                color: #000;
                box-shadow: none;
                text-decoration: none;*/
                width: 100%;
                background: #0087B4;
            }

            .dropdown-menu-header{
                top: 45px;
                padding: 10px;
                background: #000;
                border-top: 3px solid #0087B4;
                border-radius: none;
            }

            .searchbox-header{
                height: 45px;
                border-radius: 5px;
                margin-bottom: 10px;
            }

            .searchbutton{
                top: 22px;
                font-size: 20px;
                right: 10px;
            }

            .menu-mobile{
                display: none;
            }
            .sidebar-open-button-mobile{
                float: right;
            }

            @media screen and (max-width : 1024px){
                .searchform{
                    display: block;
                }
            }
            @media screen and (max-width : 980px){
                /*Make dropdown links appear inline*/
                .top-right {
                    position: static;
                    display: none;
                }
                /*Display 'show menu' link*/
                .menu-mobile {
                    display:block;
                    text-transform: uppercase;
                }
                .dropdown-mobile-menu{
                    width: 100%;
                    padding: 0;
                    border-radius: 0;
                }
                .dropdown-mobile-menu li{
                    border: 1px solid #f5f5f5;
                }
            }
        </style>
        <!-- Start Page Loading -->
        <div class="loading"><img src="<?= base_url("resources/img/loading.gif"); ?>" alt="loading-img"></div>
        <!-- End Page Loading -->
        <!-- //////////////////////////////////////////////////////////////////////////// --> 
        <!-- START TOP -->
        <div id="top" class="clearfix">
            <?php
            if (base_url() == 'http://innolabs.id') {
                $base_link = 'http://innolabs.id';
            } else if (base_url() == 'http://innolabs.id/en') {
                $base_link = 'http://innolabs.id/en';
            } else {
                $base_link = base_url('../');
            }
            ?>
            <!-- Start App Logo -->
            <div class="applogo">
                 <a href="<?= $base_link; ?>" class="logo"><img src="<?= base_url("resources/img/logo-only.png"); ?>"></img></a>
            </div>

            <!-- Start Top Right -->
			<!--
            <ul class="top-right">
                <li class="link">
                    <a href="<?= $base_link . '/index.php/home-integrated' ?>">Halaman Depan</a>
                </li>
                <li class="link">
                    <a href="<?= $base_link . '/index.php/company'; ?>">Tentang Kami</a>
                </li>
                <li class="link">
                    <a href="<?= $base_link . '/index.php/product-cartenz'; ?>">Produk & Jasa/Layanan </a>
                </li>
                <li class="link">
                    <a href="<?= base_url('index.php/casestudy'); ?>">Studi Kasus</a>
                </li> 
                <li class="link">
                    <a href="<?= $base_link . '/index.php/career'; ?>">Karir</a>
                </li>    
                <li class="link">
                    <a href="<?= $base_link . '/index.php/contact-us'; ?>">Hubungi Kami</a>
                </li>
                <li class="link">
                    <a href="<?= base_url('index.php/event'); ?>">Pusat Pelatihan</a>
                </li> 
                <li class="dropdown link">
                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-search"></i></a>
                    <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right dropdown-menu-header">
                        <li>
                            <form class="searchform">
                                <input type="text" class="searchbox searchbox-header" id="searchbox" placeholder="Search">
                                <span class="searchbutton"><i class="fa fa-search"></i></span>
                            </form>
                        </li>
                    </ul>
                </li>    
            </ul>
			-->
            <!-- End Top Right -->
            <!-- menu mobile -->
	<!--
            <div class="menu-mobile">
                <a href="#" class="sidebar-open-button-mobile" data-toggle="dropdown" class="dropdown-toggle"><i class="fa fa-bars"></i></a>
                <ul class="dropdown-menu dropdown-menu-list dropdown-menu-right dropdown-mobile-menu">
                    <li class="list-unstyle">
                        <a href="<?= $base_link . '/index.php/home-integrated' ?>">Halaman Depan</a>
                    </li>
                    <li class="list-unstyle">
                        <a href="<?= $base_link . '/index.php/company'; ?>">Tentang Kami</a>
                    </li>
                    <li class="list-unstyle">
                        <a href="<?= $base_link . '/index.php/product-cartenz'; ?>">Produk & Jasa/Layanan</a>
                    </li>
                    <li class="list-unstyle">
                        <a href="<?= base_url('index.php/casestudy'); ?>">Studi Kasus</a>
                    </li> 
                    <li class="list-unstyle">
                        <a href="<?= $base_link . '/index.php/career'; ?>">Karir</a>
                    </li>    
                    <li class="list-unstyle">
                        <a href="<?= $base_link . '/index.php/contact-us'; ?>">Hubungi Kami</a>
                    </li>
                    <li class="list-unstyle">
                        <a href="<?= base_url('index.php/event'); ?>">Pusat Pelatihan</a>
                    </li> 
                </ul> 
            </div>
            <!-- end menu mobile -->
        </div>
        <!-- END TOP -->
        <!-- //////////////////////////////////////////////////////////////////////////// --> 